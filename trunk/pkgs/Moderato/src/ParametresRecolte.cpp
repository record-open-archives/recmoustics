/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "ParametresRecolte.hpp"

using namespace std;

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace po = boost::program_options;

namespace Moderato {

    void ParametresRecolte::init(const string paramfile, const vd::Time& time)
    {
        ifstream config(paramfile.c_str());

        if (not config) {
            throw vu::ModellingError(
                vle::fmt(_("ParametresRecolte - cannot"	\
                           " open file : %1%"))
                % paramfile);
        }

        po::options_description desc("Options");
        desc.add_options()
            ("Parametres999.DateMaxi", po::value<string>(), "")
            ("Parametres999.SommeTemperature", po::value<double>(), "")
            ("Parametres999.HumiditeGrain", po::value<double>(), "")
            ("Parametres999.SommePluie", po::value<double>(), "")
            ("Parametres999.nbJours", po::value<int>(), "")
            ("Parametres999.Frut", po::value<double>(), "")
            ("Parametres999.", po::value<int>(), "");

        po::variables_map vm;
        po::store(po::parse_config_file(config, desc, true), vm);
        po::notify(vm);

        std::string section = "Parametres999.";

        DateLimiteRecolte = toTime(vm[section + "DateMaxi"].as<string>(),
                                   time);

        SommeTemperature = vm[section + "SommeTemperature"].as<double>();
        HumiditeGrain = vm[section + "HumiditeGrain"].as<double>();
        SommePluie = vm[section + "SommePluie"].as<double>();
        nbJours = vm[section + "nbJours"].as<int>();
        Frut = vm[section + "Frut"].as<double>();
        IndicateurSol =  vm[section + "nbJours"].as<int>();
    };

} // namespace Moderato


/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_FAITS_HPP
#define MODERATO_FAITS_HPP

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include "Utils.hpp"

namespace vv = vle::value;
namespace vd = vle::devs;

namespace Moderato {
/**
 * @brief a fact manager used by the Moderato agent and the decision rules to
 * plan the activities.
 */
class Faits
{
    /**
     * @brief a filter objet to provide the concept of efficiency of the
     * rain in order to compute a delay due to the rain.
     */
    class Filtre
    {
    public:

        Filtre(double efficacite) : efficacite(efficacite)
        { };

        virtual ~Filtre(){ };

        int operator()(int x, int y)
        {
            if ( y > efficacite) {
                return x + y;
            } else {
                return x;
            }
        }

    private:

        double efficacite;
    };

public:

    Faits() : STADE(AUCUN), Pluies(10, 0.), ETP(10, 0.),
              dateRecolte(vd::Time::infinity),
              PluiesRetard(10, 0.), irrigationSemis(0.),
              dateIrrigSemis(vd::Time::infinity)
    { };

    virtual ~Faits(){ };

    void stade(const vv::Value& value);

    void pluie(const vv::Value& value);

    void etp(const vv::Value& value);

    void stsemis(const vv::Value& value);

    void humiditegrain(const vv::Value& value);

    void frut(const vv::Value& value);

    void deficitsaturationeau(const vv::Value& value);

    void deficitsaturationpmax(const vv::Value& value);

    void nbjourseffectifs(const vv::Value& value);

    void volumerestant(const vv::Value& value);

    void nbjoursdepuisdbtirrigation(const vv::Value& value);

    void datesemis(const vv::Value& value);

    void daterecolte(const vv::Value& value);

    void dateirrigsemis(const vv::Value& value);

    void resetretardpluie(const vv::Value& value);

    void semis(const vv::Value& value);

    double getretardpluie(double efficacite, int duree) const;

    double getnbjoursretard(double efficacite, int duree,
                            double ratio) const;

    int getnbjoursapressemis(double date) const;

    void vent(const vv::Value& value);

    STADEPHYSIO STADE;

    std::vector < double > Pluies;
    std::vector < double > ETP;
    double Vent;

    double StSemis;
    double HumiditeGrain;

    double Frut;
    double DeficitSaturationEau;
    double DeficitSaturationPmax;

    double nbJoursEffectifs;
    double VolumeRestant;
    double nbJoursDepuisDbtIrrigation;
    double dateSemis;
    double dateRecolte;

    std::vector < double > PluiesRetard;

    double irrigationSemis; //< The state of
    double dateIrrigSemis; //< The date the irrigtion semis could be launched
};

} // namespace Moderato

#endif


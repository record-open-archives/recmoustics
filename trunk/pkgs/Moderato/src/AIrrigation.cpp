/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/devs/DynamicsDbg.hpp>

#include <vle/utils/i18n.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>

#include "Utils.hpp"
#include "ParametresIrrigation.hpp"
#include "ParametresContexte.hpp"
#include "ParametresBase.hpp"

using namespace std;
using namespace boost::algorithm;

using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension;
namespace va = vle::extension::fsa;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

#define NB_JOURS_SEMAINE 7
#define NB_CONTRAINTES 6

namespace Moderato
{
/**
 * @brief a irrigation operation vle::extension::fsa::Statechart model
 *
 * A irrigation model based on
 * the statechart formalism.
 * As soon as the model receive an event(order) on the port
 * "Start"; ...
 *
 *
 * the list of ports :
 * - Experimental condition :
 *  - nomFichierSemis [String] relative file path of the config file
 * - Input :
 *  - Start receiving an empty event.
 * - Output :
 *  - ack : sending an event with 2 attributes :
 *   - name [String] "Semis"
 *   - value [String] "done"
 *  - Fertilisation : sending a "Semis" Difference equation
 * value.
 * - Observation :
 *  - State
 */
class AIrrigation: public va::Statechart
{
public:
    enum State { INIT, BEFORE, IN_PROGRESS, DELAYED, DUMMY, BETWEEN};

    AIrrigation(const vd::DynamicsInit& atom,
                const vd::InitEventList& events)
        : va::Statechart(atom, events), IrrigationCourante(0),
          nbJoursDepuisDbtTourEau(0),nbJoursEffectifs(0),
          isJour(false), isFin(false), isTourEauCommence(false),
          nbJoursAvantIrrigation(3, -1), QuantiteIrrigationApportee(.0),
          DebitCourant(0), TauxCourant(.0), LTE(.0), nbJoursRetardPluie(0),
          mIrrigation("N")
    {
        // gestion des conditions expérimentales
        // qui permettent de charger les paramètres.

        string nomFichier = toString(events.get("nomFichierIrrigation"));
        filepathIrrigation = vu::Path::path().getPackageDataFile(nomFichier);

        nomFichier = toString(events.get("nomFichierContexte"));
        filepathContexte = vu::Path::path().getPackageDataFile(nomFichier);

        nomFichier = toString(events.get("nomFichierBase"));
        filepathBase = vu::Path::path().getPackageDataFile(nomFichier);

        // construction de l'automate

        states(this) << INIT << BEFORE << IN_PROGRESS << DUMMY << DELAYED << BETWEEN;

        transition(this, INIT, BEFORE)
            << action(&AIrrigation::initState);

        transition(this, BEFORE, DUMMY)
            << guard(&AIrrigation::positionAIrriguer)
            << action(&AIrrigation::doStart)
            << send(&AIrrigation::out);

        eventInState(this, "Start", &AIrrigation::inStart) >> BEFORE;

        transition(this, IN_PROGRESS, DUMMY)
            << guard(&AIrrigation::positionAIrriguer)
            << action(&AIrrigation::doPosition)
            << send(&AIrrigation::out);

        transition(this, IN_PROGRESS, DELAYED)
            << guard(&AIrrigation::isDelay)
            << send(&AIrrigation::ackDelay);

        transition(this, DELAYED, DELAYED)
            << after(vu::DateTime::aDay())
            << action(&AIrrigation::doDelay)
            << send(&AIrrigation::info);

        transition(this, DELAYED, IN_PROGRESS)
            << guard(&AIrrigation::isFinDelay)
            << action(&AIrrigation::doFinDelay);

        transition(this, IN_PROGRESS, IN_PROGRESS)
            << after(vu::DateTime::aDay())
            << action(&AIrrigation::inTourEau)
            << send(&AIrrigation::info);

        transition(this, DUMMY, IN_PROGRESS)
            << action(&AIrrigation::positionIrriguee);

        transition(this, IN_PROGRESS, BETWEEN)
            << guard(&AIrrigation::isFinTourEau)
            << action(&AIrrigation::doFinTourEau)
            << send(&AIrrigation::outputFinTourEau);

        transition(this, IN_PROGRESS, BETWEEN)
            << event("Stop")
            << action(&AIrrigation::doFinTourEau)
            << send(&AIrrigation::outputFinTourEau);

        transition(this, BETWEEN, BETWEEN)
            << after(vu::DateTime::aDay())
            << action(&AIrrigation::outTourEau)
            << send(&AIrrigation::info);

        transition(this, BETWEEN, DUMMY)
            << guard(&AIrrigation::debutIrrigation)
            << action(&AIrrigation::doStart)
            << send(&AIrrigation::out);

        eventInState(this, "Delay", &AIrrigation::inDelay) >> IN_PROGRESS;

        eventInState(this, "Delay", &AIrrigation::inDelay) >> DUMMY;

        eventInState(this, "Delay", &AIrrigation::inDelay) >> DELAYED;

        eventInState(this, "Start", &AIrrigation::inStart) >> BETWEEN;

        timeStep(vu::DateTime::aDay());

        initialState(INIT);
    }

    virtual ~AIrrigation()
    {
    }

private:

    void initState(const vd::Time& time)
    {
        mIrrigation = "N";

        parametresContexte.init(filepathContexte,time);
        parametresIrrigation.init(filepathIrrigation,time);
        parametresBase.init(filepathBase);

        if(isRenseignee(parametresContexte.VolumeTotal)) {
            VolumeRestant = parametresContexte.VolumeTotal;
        }

        int iContexte =  parametresContexte.getContext(time);

        DebitCourant = parametresContexte.mesContraintes[iContexte].Debit;
        TauxCourant = parametresContexte.mesContraintes[iContexte].Taux;
    }

    void inTourEau(const vd::Time& /*time*/)
    {
        mIrrigation = "I";

        nbJoursAvantIrrigation[0]-- ;
        nbJoursAvantIrrigation[1]-- ;
        nbJoursAvantIrrigation[2]-- ;
        nbJoursDepuisDbtTourEau++;
        nbJoursEffectifs++;
    }

    void outTourEau(const vd::Time& /*time*/)
    {
        nbJoursEffectifs++;
    }

    bool debutTourEau(const vd::Time& julianDay)
    {
        GererTourEau(julianDay, false);
        return isTourEauCommence;
    }

    bool isFinTourEau(const vd::Time& /*julianDay*/)
    {
        return nbJoursDepuisDbtTourEau > LTE || isFin;
    }

    bool isDelay(const vd::Time& /*julianDay*/)
    {
        return nbJoursRetardPluie > 0;
    }

    bool isFinDelay(const vd::Time& /*julianDay*/)
    {
        return nbJoursRetardPluie == 0;
    }

    void doFinTourEau(const vd::Time& /*julianDay*/)
    {
        mIrrigation = "N";

        LTE = 0;
        isTourEauCommence = false;
        nbJoursDepuisDbtTourEau = 0;
    }

    void doDelay(const vd::Time& /*julianDay*/)
    {
        mIrrigation = "R" + lexical_cast<string>(nbJoursRetardPluie);

        nbJoursRetardPluie--;
        nbJoursDepuisDbtTourEau++;
    }

    void doFinDelay(const vd::Time& /*julianDay*/)
    {
        //mIrrigation = "R" + lexical_cast<string>(nbJoursRetardPluie);
    }

    void doStart(const vd::Time& /*julianDay*/)
    {
        mIrrigation = str(boost::format("0[%d, %d, %d, %d, %d]")
                          % DebitCourant
                          % TauxCourant
                          % QuantiteIrrigationApportee
                          % VolumeRestant
                          % LTE);
    }

    void doPosition(const vd::Time& /*julianDay*/)
    {
        int position = numAIrriguer() - 1;
        if (position == 0) {
            mIrrigation = "I";
        } else {
            mIrrigation = lexical_cast<string>(numAIrriguer() - 1);
        }
    }

    bool positionAIrriguer(const vd::Time& /*julianDay*/)
    {
        return nbJoursAvantIrrigation[0] == 0 ||
            nbJoursAvantIrrigation[1] == 0 ||
            nbJoursAvantIrrigation[2] == 0 ;
    }

    void positionIrriguee(const vd::Time& /*time*/)
    {
        nbJoursAvantIrrigation[numAIrriguer() - 1] = -1;
    }

    void GererTourEau(const vd::Time& time, bool isIA)
    {
        isTourEauCommence = true;
        int iContexte =  parametresContexte.getContext(time);

        DebitCourant = parametresContexte.mesContraintes[iContexte].Debit;
        TauxCourant = parametresContexte.mesContraintes[iContexte].Taux;

        // S'il ne reste pas suffisament d'eau on réduit la dose à
        // apporter
        if(isRenseignee(parametresContexte.VolumeTotal)) {
            if( VolumeRestant < QuantiteIrrigationApportee) {
                QuantiteIrrigationApportee = VolumeRestant;
            }
        }

        //Si la dose à appliquer est inférieure à la dose minimale autorisee,
        //on ne declenche pas l'irrigation, sauF si on est en approche IA
        if(!isIA) {
            if(QuantiteIrrigationApportee < parametresContexte.DoseMinimale) {
                isTourEauCommence = false;
                for(int iNbPositions = 0;
                    iNbPositions < parametresBase.nbPositions;
                    iNbPositions++)
                    nbJoursAvantIrrigation[iNbPositions] = -1;
            }
        }

        //Si la dose à appliquer est supérieure à la dose maximale autorisee,
        //on réduit la dose
        if(QuantiteIrrigationApportee > parametresContexte.DoseMaximale) {
            QuantiteIrrigationApportee = parametresContexte.DoseMaximale;
        }

        //Calcule la longueur du tour d'eau
        LTE = ceil(QuantiteIrrigationApportee / (DebitCourant * (TauxCourant / 24.)));

        //Le LTE ne peut pas etre plus petit que 3 si on travaille sur un bloc
        if(parametresBase.nbPositions == 3)
        {
            if(LTE < 3) {
                LTE = 3;
            }
        }

        //Soustrait la quantite apportee au volume total
        if(isRenseignee(parametresContexte.VolumeTotal))
        {
            //Dans le cas de l'irrigation de Semis, on vérifie si on doit retirer
            if (mActivity == "IrrigationSemis")
            {
                if (parametresIrrigation.isARetirerSemis) {
                    VolumeRestant -= QuantiteIrrigationApportee;
                }
            } else
                VolumeRestant -= QuantiteIrrigationApportee;
            if(VolumeRestant == 0)
            {
                isFin = true;
            }
	}

        //Renseigne la structure de transfert pour chaque position

        if(parametresBase.nbPositions == 3) {
            nbJoursAvantIrrigation[0] = 0;
            nbJoursAvantIrrigation[1] = ceil(LTE / 2) + ((int)LTE % 2) - 1;
            nbJoursAvantIrrigation[2] = LTE - 1;
        } else {
            nbJoursAvantIrrigation[0] = 0;
        }

        IrrigationCourante++;
        nbJoursDepuisDbtTourEau = 1;
        nbJoursEffectifs = 1;
        isJour = true;
    }

    bool debutIrrigation(const vd::Time& /*julianDay*/)
    {
        return isTourEauCommence;
    }

    int numAIrriguer() const
    {
        if (nbJoursAvantIrrigation[0] == 0) {
            return 1;
        } else if (nbJoursAvantIrrigation[1] == 0) {
            return 2;
        } else { // nbJoursAvantIrrigation[0] == 2 {
            return 3;
        }
    }

    void out(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
    {
        string portAIrriguer = str(boost::format("Irrigation_%1$d") %
                                           numAIrriguer());

	vd::ExternalEvent* order = new vd::ExternalEvent(portAIrriguer);
        order->putAttribute("name", new vv::String("Irrigation"));
        order->putAttribute("value", new vv::Double(QuantiteIrrigationApportee));

        output.addEvent(order);

        if (mActivity == "IrrigationSemis") {
            output << (ve::DifferenceEquation::Var("Semis") = 1.0);
        }
    }

    void info(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
    {
        output << (ve::DifferenceEquation::Var("nbJoursDepuisDbtTourEau") = nbJoursDepuisDbtTourEau);
        output << (ve::DifferenceEquation::Var("nbJoursEffectifs") = nbJoursEffectifs);
    }

    /**
     * @brief output information after a irrigation round
     */
    void outputFinTourEau(const vd::Time& julianDay, vd::ExternalEventList& output) const
    {
        //std::cout << "[Application Irrigation]: outputFinTourEau(ack) " << mActivity <<std::endl;

        info(julianDay, output);

        if (mActivity == "IrrigationSemis") {
            // to mention that the IrrigationSemis has been done.
            output << (ve::DifferenceEquation::Var("Semis") = 2.0);
        }

        // acknowledge the last activity
        vd::ExternalEvent* order = new vd::ExternalEvent("ack");
        order->putAttribute("name", new vv::String(mActivity));
        order->putAttribute("value", new vv::String("done"));
        output.addEvent(order);
    }

    void ackDelay(const vd::Time& julianDay, vd::ExternalEventList& output) const
    {
        info(julianDay, output);

        vd::ExternalEvent* order = new vd::ExternalEvent("ack");
        order->putAttribute("name", new vv::String(mActivityDelay));
        order->putAttribute("value", new vv::String("done"));
        output.addEvent(order);
    }

    void inStart(const vd::Time& julianDay, const vd::ExternalEvent* evt)
    {
        mActivity = evt->getStringAttributeValue("activity");

        QuantiteIrrigationApportee = evt->getDoubleAttributeValue("QuantiteAppliquee");

        GererTourEau(julianDay, false);

        if(isTourEauCommence)
        {
            isDebutIrrigation = 1;
            isIrrigationCommencee = TRUE;
        }
    }

    void inDelay(const vd::Time& /*julianDay*/, const vd::ExternalEvent* evt)
    {
        mActivityDelay = evt->getStringAttributeValue("activity");

        nbJoursRetardPluie = evt->getDoubleAttributeValue("nbJoursRetard");
        LTE += nbJoursRetardPluie;
    }

    /**
     * @brief observation du port Operation
     */
    virtual vv::Value* observation(const vd::ObservationEvent & event ) const
    {
        if ( event.onPort("LTE")) {
            return vv::Double::create(LTE);
        } else if (event.onPort("QuantiteIrrigationApportee")) {
            return vv::Double::create(QuantiteIrrigationApportee);
        } else if (event.onPort("DebitCourant")) {
            return vv::Double::create(DebitCourant);
        } else if (event.onPort("TauxCourant")) {
            return vv::Double::create(TauxCourant);
        } else if (event.onPort("IrrigationCourante")) {
            return vv::Integer::create(IrrigationCourante);
        } else if (event.onPort("nbJoursDepuisDbtTourEau")) {
            return vv::Integer::create(nbJoursDepuisDbtTourEau);
        } else if (event.onPort("nbJoursEffectifs")) {
            return vv::Integer::create(nbJoursEffectifs);
        } else if (event.onPort("VolumeRestant")) {
            return vv::Double::create(VolumeRestant);
        } else if (event.onPort("nbJoursRetardPluie")) {
            return vv::Integer::create(nbJoursRetardPluie);
        } else if (event.onPort("Position")) {
            return vv::Integer::create(numAIrriguer());
        } else if (event.onPort("Irrigation")) {
            return vv::String::create(mIrrigation);
        } else { //event.onPort("state")
            return va::Statechart::observation(event);
        }
    }

    // Variables d'états
    int IrrigationCourante;
    int nbJoursDepuisDbtTourEau;
    int nbJoursEffectifs;
    bool isJour;
    bool isFin;
    bool isTourEauCommence;
    double VolumeRestant;
    int isDebutIrrigation;
    bool isIrrigationCommencee;
    std::vector < int > nbJoursAvantIrrigation;

    // Paramètres calculés
    double Dose;
    // Calculé par RDD
    double QuantiteIrrigationApportee;
    double DebitCourant;
    double TauxCourant;
    double LTE;

    int nbJoursRetardPluie;

    //
    std::string mActivity;
    std::string mActivityDelay;

    string filepathIrrigation;
    string filepathContexte;
    string filepathBase;

    ParametresIrrigation parametresIrrigation;
    ParametresContexte parametresContexte;
    ParametresBase parametresBase;

    std::string mIrrigation;
};

} //namespace Moderato

DECLARE_DYNAMICS(Moderato::AIrrigation)
DECLARE_NAMED_DYNAMICS_DBG(AIrrigationDbg,Moderato::AIrrigation)

/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_PARAMETRESBASE_HPP
#define MODERATO_PARAMETRESBASE_HPP

#include <string>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include <boost/program_options.hpp>

#include "Utils.hpp"

namespace vu = vle::utils;
namespace vd = vle::devs;

namespace Moderato {
/**
 * @brief Parameters and values of the base
 *
 * This class is a simple structure containing all the base
 * parameters, and their values used by some models if the simulator.
 */
class ParametresBase
{
public:

    ParametresBase() :
        nbPositions(0)
    { };

    virtual ~ParametresBase() { };
    /**
     * @brief Setting of the base parameters
     *
     * @param paramfile is the complete path to the config file
     */
    void init(const std::string paramfile);

    int nbPositions;
};

} // namespace Moderato

#endif


/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "DecisionFertilisation.hpp"

namespace Moderato {

    void DecisionFertilisation::init(const std::string paramfile)
    {
        parametres.init(paramfile);
    }

    bool DecisionFertilisation::stade() const
    {
        return faits.STADE >= parametres.STADE;
    }

    void DecisionFertilisation::RdDDebut999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("RdDFertilisation");

        r1.add(boost::bind(&DecisionFertilisation::stade, this));
    }
} // namespace Moderato


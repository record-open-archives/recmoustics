/**
 * @file
 * @author The RECORD Development Team (INRA)
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 *
 */

/*
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>

#include "Utils.hpp"

using namespace std;
using namespace boost;

namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

/**
 * @brief a model to cumulate state variables.
 *
 * in order to facilitate Outputs.
 *
 */
namespace Moderato {

class Cumul : public vle::extension::DifferenceEquation::Multiple
{
public:
    Cumul(const vle::devs::DynamicsInit& init,
          const vle::devs::InitEventList& events) :
	vle::extension::DifferenceEquation::Multiple(init, events),
        DateRecolte(vd::Time::negativeInfinity)
    {
        DefHydPreFlo = createVar("DefHydPreFlo");
        DefHydFlo = createVar("DefHydFlo");
        DefHydPostFlo = createVar("DefHydPostFlo");

        STADE = createSync("STADE");
        ETP = createSync("ETP");
        Rain = createSync("Rain");

        Recolte = createVar("Recolte");
    }

    virtual ~Cumul() { }

    virtual void compute(const vle::devs::Time& time)
    {
        Recolte = 0.0;

        if (Recolte() != 0.0) {
            DateRecolte = time;
        }

        STADEPHYSIO stade = (STADEPHYSIO) STADE();

        if (stade ==  SEMIS_LEVEE || stade ==
            LEVEE_FINCROISFEUIL) {
            DefHydPreFlo = DefHydPreFlo(-1) + ETP() - Rain();
        } else {
            DefHydPreFlo = DefHydPreFlo(-1);
        }

        if (stade == FINCROISFEUIL_SORTIESOIES ||
            stade == SORTIESOIES_AVORTGRAIN) {
            DefHydFlo = DefHydFlo(-1) + ETP() - Rain();
        }  else {
            DefHydFlo = DefHydFlo(-1);
        }
        if (stade >= AVORTGRAIN_DBTSEN &&
            time <= DateRecolte) {
            DefHydPostFlo = DefHydPostFlo(-1) + ETP() - Rain();
        }  else {
            DefHydPostFlo = DefHydPostFlo(-1);
        }
    }

    virtual void initValue(const vle::devs::Time& /*time*/)
    {
        DefHydPreFlo = 0.0;
        DefHydFlo = 0.0;
        DefHydPostFlo = 0.0;
    }

private:

    Var DefHydPreFlo;
    Var DefHydFlo;
    Var DefHydPostFlo;

    Sync STADE;
    Sync ETP;
    Sync Rain;

    Var Recolte;
    vd::Time DateRecolte;
};

} //namespace Moderato

DECLARE_DYNAMICS(Moderato::Cumul)

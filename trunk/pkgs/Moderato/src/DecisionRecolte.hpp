/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_DECISIONRECOLTE_HPP
#define MODERATO_DECISIONRECOLTE_HPP

#include <vle/extension/Decision.hpp>

#include "Faits.hpp"
#include "ParametresRecolte.hpp"

namespace ve = vle::extension::decision;

namespace Moderato {

/**
 * @brief predicates and decision rules of the harvesting used by Moderato
 *
 * This object is hosting all the predicates necessary to the decision
 * rule. It also contains the parameters and it has an access to the
 * facts the decision depends on.  This object also build the decision
 * by agregating predicates.
 */
class DecisionRecolte
{
public:

    DecisionRecolte(const Faits& f, const ve::Agent& a) : faits(f), agent(a)
    { };

    ~DecisionRecolte() { };

    void init(const std::string paramfile, const vd::Time& time);

    /**
     * @brief predicate
     */
    bool dateLimiteRecolte() const;
    /**
     * @brief predicate
     */
    bool sommeTemperature() const;
    /**
     * @brief predicate
     */
    bool humiditeGrain() const;
    /**
     * @brief predicate
     */
    bool portanceMeteo() const;
    /**
     * @brief predicate
     */
    bool portanceSol1() const;
    /**
     * @brief predicate
     */
    bool portanceSol2() const;
    /**
     * @brief predicate
     */
    bool portanceSol() const;

    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDDebut999(ve::Activity& a) const;

    ParametresRecolte parametres; //!< access to the parameters

private:

    const Faits& faits; //!< access to facts used to take a decision
    const ve::Agent& agent; //!< access to time of the simulation
};
} // namespace Moderato

#endif

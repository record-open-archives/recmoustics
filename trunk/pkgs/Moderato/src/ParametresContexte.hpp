/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_PARAMETRESCONTEXTE_HPP
#define MODERATO_PARAMETRESCONTEXTE_HPP

#include <string>
#include <boost/algorithm/string/split.hpp>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include <boost/program_options.hpp>

#include "Utils.hpp"

#define NB_JOURS_SEMAINE 7
#define NB_CONTRAINTES 6

using namespace boost::algorithm;
namespace vu = vle::utils;
namespace vd = vle::devs;

namespace Moderato {
/**
 * @brief Parameters and values of the context
 *
 * This class is a simple structure containing all the parameters,
 * and their values defining the context of irrigation.
 */
class ParametresContexte
{
public:
    ParametresContexte() { };

    virtual ~ParametresContexte() { };

    void init(const std::string paramfile, const vd::Time& time);

    /**
     * @brief Parameters and values of the context for a single
     * period(constraint).
     */
    struct TContraintes{
        int Numero;
        vd::Time DateDebut;
        vd::Time DateFin;
        double Debit;
        double Taux;
        bool JoursSansIrrigation[NB_JOURS_SEMAINE];
    };

    /**
     * @brief Predicate to access the period(constraint) of a context
     * according to the time.
     */
    struct in_context
    {
        const vd::Time& value;

        in_context(const vd::Time& value):
            value(value)
        {}

        bool operator() (const TContraintes& elem) const
        {
            return (elem.DateDebut <= value && value <= elem.DateFin);
        }
    };

    /**
     * @brief to get the period(constraint) of a context according to
     * the time.
     *
     * @param time the current time
     * @return the index of a period.
     */
    int getContext(const vle::devs::Time& time) const;

    int Position;

    double DoseMinimale;
    double DoseMaximale;
    double VolumeTotal;

    std::vector< TContraintes > mesContraintes;

    vd::Time DateDebut;
    double SommeTempDebut;
    bool isOU_ET;
    double SommeTempDebutMax;
    int qCondDebut;
    int IndicateurSolDebut;
    double ValeursPluieDebut[3];
    double ValeursFrutDebut[7];
    int qDoseDebut;
    int IndicateurDoseDebut;
    double DoseDebut;
};

} // namespace Moderato

#endif


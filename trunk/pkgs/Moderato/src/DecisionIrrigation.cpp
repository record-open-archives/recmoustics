/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "DecisionIrrigation.hpp"

namespace Moderato {

    void DecisionIrrigation::init(const std::string paramfile,
                                  const vd::Time& time)
    {
        parametres.init(paramfile, time);
    }

    //
    // Predicates
    //

    bool DecisionIrrigation::notSommeTemperatureMax() const
    {
        return !(faits.StSemis > parametres.SommeTempDebutMax);
    }

    bool DecisionIrrigation::dateDebutIrrigation() const
    {
        return agent.currentTime() >= parametres.DateDebut;
    }

    bool DecisionIrrigation::sommeTemperature() const
    {
        return faits.StSemis >= parametres.SommeTempDebut;
    }

    bool DecisionIrrigation::sommePluie() const
    {
        return accumulate(faits.Pluies.begin(),
                          faits.Pluies.begin() + parametres.ValeursPluieDebut[2] - 1,
                          0.) < parametres.ValeursPluieDebut[0];
    }

    bool DecisionIrrigation::sommeETP() const
    {
        return accumulate(faits.ETP.begin(),
                          faits.ETP.begin() + parametres.ValeursPluieDebut[2]- 1,
                          0.) >= parametres.ValeursPluieDebut[1];
    }

    bool DecisionIrrigation::conditionSol1() const
    {
        double frut = calcFrut();
        return frut >= faits.Frut;
    }

    bool DecisionIrrigation::conditionSol2() const
    {
        double frut = calcFrut();
        return frut <= faits.DeficitSaturationEau;
    }

    bool DecisionIrrigation::conditionSol() const
    {
        double frut = calcFrut();
        return frut <= faits.DeficitSaturationPmax;
    }

    bool DecisionIrrigation::periodeFixe() const
    {
        return faits.nbJoursEffectifs >= parametres.Tab1_1[0] + 1;
    }

    bool DecisionIrrigation::periodeModuleeSimple() const
    {
        int PeriodiciteBasse = 0;
        int PeriodiciteMoyenne = 0;
        int PeriodiciteHaute = 0;
        double valBasse = 0;
        double valHaute = 0;
        int valJour = 0;
        int n = 0;

        choixPeriodicite(PeriodiciteBasse, PeriodiciteMoyenne, PeriodiciteHaute,
                         valBasse, valHaute, valJour);

        if(parametres.isModulation) {
            n = (int)clcDeltaPeriodicite();
        }

        return faits.nbJoursEffectifs >= PeriodiciteBasse + 1 + n;
    }

    bool DecisionIrrigation::periodeModulee() const
    {
        int PeriodiciteBasse = 0;
        int PeriodiciteMoyenne = 0;
        int PeriodiciteHaute = 0;
        double valBasse = 0;
        double valHaute = 0;
        int valJour = 0;
        int n = 0;
        double valeur = 0;

        choixPeriodicite(PeriodiciteBasse, PeriodiciteMoyenne, PeriodiciteHaute,
                         valBasse, valHaute, valJour);

        if(parametres.isModulation) {
            n = (int)clcDeltaPeriodicite();
        }

        if(parametres.qCond2Retour == 2) {
            valeur = (valJour) ?
                accumulate(faits.ETP.begin(),
                           faits.ETP.begin() + (valJour - 1),
                           0.) / valJour :
                accumulate(faits.ETP.begin(),
                           faits.ETP.begin() + 0,
                           0.);
        } else {
            if(parametres.IndicateurSolRetour == 1) {
                valeur = 1. - faits.Frut;
                valBasse = 1. - valBasse;
                valHaute = 1. - valHaute;
            } else if (parametres.IndicateurSolRetour == 2) {
                valeur = faits.DeficitSaturationEau;
            } else {
                valeur = faits.DeficitSaturationPmax;
            }
        }

        if(valeur < valBasse) {
            return faits.nbJoursEffectifs >= PeriodiciteBasse + 1 + n;
        }
        else if(valeur < valHaute) {
            return faits.nbJoursEffectifs >= PeriodiciteMoyenne + 1 + n;
        } else {
            return faits.nbJoursEffectifs >= PeriodiciteHaute + 1 + n;
        }
    }

    bool DecisionIrrigation::pSolRetour1() const
    {
        double valeur = choixValeur();
        return valeur > faits.Frut;
    }

    bool DecisionIrrigation::pSolRetour2() const
    {
        double valeur = choixValeur();
        return valeur < faits.DeficitSaturationEau;
    }

    bool DecisionIrrigation::pSolRetour() const
    {
        double valeur = choixValeur();
        return valeur < faits.DeficitSaturationPmax;
    }

    bool DecisionIrrigation::pSommePluieIrrigSemis() const
    {
        return accumulate(faits.Pluies.begin(),
                          faits.Pluies.begin() + faits.nbJoursDepuisDbtIrrigation,
                          0.) > parametres.ArretSemis;
    }

    bool DecisionIrrigation::pNbJoursIrrigSemis() const
    {
        return faits.getnbjoursapressemis(agent.currentTime()) == parametres.nbJoursSemis;
    }

    bool DecisionIrrigation::pSomTempIrrigSemis() const
    {
        return faits.StSemis >= parametres.SommeTempSemis;
    }

    bool DecisionIrrigation::pSomPluieIrrigSemis() const
    {
        return parametres.SommePluieSemis > accumulate(faits.Pluies.begin(),
                                                       faits.Pluies.begin(),
                                                       faits.getnbjoursapressemis(agent.currentTime()));
    }

    bool DecisionIrrigation::pFrutIrrigSemis() const
    {
        return parametres.FrutSemis >= faits.Frut;
    }

    bool DecisionIrrigation::pDeficitSatEauIrrigSemis() const
    {
        return parametres.FrutSemis <= faits.DeficitSaturationEau;
    }

    bool DecisionIrrigation::pDeficitSatPmaxIrrigSemis() const
    {
        return parametres.FrutSemis <= faits.DeficitSaturationPmax;
    }

    bool DecisionIrrigation::pArretIrrigArret() const
    {
        return accumulate(faits.Pluies.begin(),
                          faits.Pluies.begin() + faits.nbJoursDepuisDbtIrrigation,
                          0.) > parametres.ArretArret;
    }

    bool DecisionIrrigation::pDateIrrigArret() const
    {
        return agent.currentTime() >= parametres.DateArret;
    }

    bool DecisionIrrigation::pNotDateIrrigArret() const
    {
        return !pDateIrrigArret();
    }

    bool DecisionIrrigation::pSomTempIrrigArret() const
    {
        return faits.StSemis >= parametres.SommeTempArret;
    }

    bool DecisionIrrigation::pNotSomTempIrrigArret() const
    {
        return !pSomTempIrrigArret();
    }

    bool DecisionIrrigation::pSomPluieSomETPIrrigArret() const
    {
        bool isPluieInferieureSeuil = false;
        bool isETPInferieureSeuil = false;

	if(accumulate(faits.Pluies.begin(),
                      faits.Pluies.begin(),
                      (int)parametres.ValeursClimatArret[0]) <
           parametres.ValeursClimatArret[1]) {
            isPluieInferieureSeuil = true;
        }

        if(accumulate(faits.ETP.begin(),
                      faits.ETP.begin(),
                      (int)parametres.ValeursClimatArret[0]) <
           parametres.ValeursClimatArret[2]) {
            isETPInferieureSeuil = true;
        }

        if(isPluieInferieureSeuil && isETPInferieureSeuil) {
            return parametres.ValeursClimatArret[3];
        } else if(isPluieInferieureSeuil && !isETPInferieureSeuil) {
            return parametres.ValeursClimatArret[4];
        } else if(!isPluieInferieureSeuil && isETPInferieureSeuil) {
            return parametres.ValeursClimatArret[5];
        } else { // if(!isPluieInferieureSeuil && !isETPInferieureSeuil) {
            return parametres.ValeursClimatArret[6];
        }
    }

    bool DecisionIrrigation::pNotSomPluieSomETPIrrigArret() const
    {
        return !pSomPluieSomETPIrrigArret();
    }

    bool DecisionIrrigation::pFrutIrrigArret() const
    {
        return parametres.FrutArret >= faits.Frut;
    }

    bool DecisionIrrigation::pNotFrutIrrigArret() const
    {
        return !pFrutIrrigArret();
    }

    bool DecisionIrrigation::pDeficitSatEauIrrigArret() const
    {
        return parametres.FrutArret <= faits.DeficitSaturationEau;
    }

    bool DecisionIrrigation::pNotDeficitSatEauIrrigArret() const
    {
        return !pDeficitSatEauIrrigArret();
    }

    bool DecisionIrrigation::pDeficitSatPmaxIrrigArret() const
    {
        return parametres.FrutArret <= faits.DeficitSaturationPmax;
    }

    bool DecisionIrrigation::pNotDeficitSatPmaxIrrigArret() const
    {
        return !pDeficitSatPmaxIrrigArret();
    }

    bool DecisionIrrigation::pPluieRetard() const
    {
        return faits.getretardpluie(parametres.ValeursPluie[0],
                                    parametres.ValeursPluie[1]) >=
            parametres.ValeursPluie[2];
    }

    bool DecisionIrrigation::pVentRetard() const
    {
        return faits.Vent > parametres.ValeursVent[0];
    }

    bool DecisionIrrigation::pRetourRetard() const
    {
        return parametres.Retour1Retard <= faits.Pluies[0];
    }

    bool DecisionIrrigation::pDateIrrigSemis() const
    {
        return faits.dateIrrigSemis == agent.currentTime();
    }

    bool DecisionIrrigation::pIrrigationSemisClear() const
    {
        if (parametres.isRdDSemis) {
            // rule set
            if (faits.dateIrrigSemis < agent.currentTime()) {
                // outside the time range
                if (faits.irrigationSemis == 1.) {
                    // irrigationSemis is progress
                    return false;
                } else {
                    // irrigationSemis either not started either done
                    return true;
                }
            }
        } else {
            // rule not set
            return true;
        }
    }

    //
    // Rules builder
    //

    void DecisionIrrigation::RdDDebut999(ve::Activity& a) const
    {
        if (!(parametres.isOU_ET)) {
            ve::Rule& r1 = a.addRule("DateDebut");
            ve::Rule& r2 = a.addRule("SommeTempDebut");

            //to make sur the irrigation semis is clear

            r1.add(boost::bind(&DecisionIrrigation::pIrrigationSemisClear, this));
            r2.add(boost::bind(&DecisionIrrigation::pIrrigationSemisClear, this));

            r1.add(boost::bind(&DecisionIrrigation::notSommeTemperatureMax, this));
            if (isRenseignee(parametres.DateDebut)) {
                r1.add(boost::bind(&DecisionIrrigation::dateDebutIrrigation, this));
            }
            r2.add(boost::bind(&DecisionIrrigation::notSommeTemperatureMax, this));
            if (isRenseignee(parametres.SommeTempDebut)) {
                r2.add(boost::bind(&DecisionIrrigation::dateDebutIrrigation, this));
            }
            if (parametres.qCondDebut == 1) {
                r1.add(boost::bind(&DecisionIrrigation::sommePluie, this));
                r1.add(boost::bind(&DecisionIrrigation::sommeETP, this));
                r2.add(boost::bind(&DecisionIrrigation::sommePluie, this));
                r2.add(boost::bind(&DecisionIrrigation::sommeETP, this));
            } else {
                if(parametres.IndicateurSolDebut == 1)
                {
                    r1.add(boost::bind(&DecisionIrrigation::conditionSol1, this));
                    r2.add(boost::bind(&DecisionIrrigation::conditionSol1, this));
                } else if(parametres.IndicateurSolDebut==2)
                {
                    r1.add(boost::bind(&DecisionIrrigation::conditionSol2, this));
                    r2.add(boost::bind(&DecisionIrrigation::conditionSol2, this));
                } else
                {
                    r1.add(boost::bind(&DecisionIrrigation::conditionSol, this));
                    r2.add(boost::bind(&DecisionIrrigation::conditionSol, this));
                }
            }
        } else {
            ve::Rule& r1 = a.addRule("DateDebut&SommeTempDebut");

            //to make sur the irrigation semis is clear
            r1.add(boost::bind(&DecisionIrrigation::pIrrigationSemisClear, this));

            r1.add(boost::bind(&DecisionIrrigation::notSommeTemperatureMax, this));

            if (isRenseignee(parametres.DateDebut)) {
                r1.add(boost::bind(&DecisionIrrigation::dateDebutIrrigation, this));
            }
            if (isRenseignee(parametres.SommeTempDebut)) {
                r1.add(boost::bind(&DecisionIrrigation::dateDebutIrrigation, this));
            }
            if (parametres.qCondDebut == 1) {
                r1.add(boost::bind(&DecisionIrrigation::sommePluie, this));
                r1.add(boost::bind(&DecisionIrrigation::sommeETP, this));
            } else {
                if(parametres.IndicateurSolDebut == 1)
                {
                    r1.add(boost::bind(&DecisionIrrigation::conditionSol1, this));
                } else if(parametres.IndicateurSolDebut==2)
                {
                    r1.add(boost::bind(&DecisionIrrigation::conditionSol2, this));
                } else
                {
                    r1.add(boost::bind(&DecisionIrrigation::conditionSol, this));
                }
            }
        }
    }

    void DecisionIrrigation::RdDRetour999(ve::Activity& a) const
    {
        if (parametres.qCondRetour == 1) {
            ve::Rule& r1 = a.addRule("RetourPeriodiciteFixe");
            ve::Rule& r2 = a.addRule("RetourPeriodiciteFixebis");
            r1.add(boost::bind(&DecisionIrrigation::periodeFixe, this));
            r2.add(boost::bind(&DecisionIrrigation::periodeFixe, this));
            NotRdDArret999(r1);
            NotRdDArret999bis(r2);
        } else if(parametres.qCondRetour == 2) {
            if(parametres.qCond2Retour == 1) {
                ve::Rule& r1 = a.addRule("RetourPeriodiciteModuleSimple");
                r1.add(boost::bind(&DecisionIrrigation::periodeModuleeSimple, this));
                ve::Rule& r2 = a.addRule("RetourPeriodiciteModuleSimplebis");
                r2.add(boost::bind(&DecisionIrrigation::periodeModuleeSimple, this));
                NotRdDArret999(r1);
                NotRdDArret999bis(r2);
            } else {
                ve::Rule& r1 = a.addRule("RetourPeriodiciteModule");
                r1.add(boost::bind(&DecisionIrrigation::periodeModulee, this));
                ve::Rule& r2 = a.addRule("RetourPeriodiciteModulebis");
                r2.add(boost::bind(&DecisionIrrigation::periodeModulee, this));
                NotRdDArret999(r1);
                NotRdDArret999bis(r2);
            }
        } else {
            ve::Rule& r1 = a.addRule("RetourDeficit");
            ve::Rule& r2 = a.addRule("RetourDeficitbis");
            if(parametres.IndicateurSolRetour == 1)
            {
                r1.add(boost::bind(&DecisionIrrigation::pSolRetour1, this));
                r2.add(boost::bind(&DecisionIrrigation::pSolRetour1, this));
            } else if(parametres.IndicateurSolRetour==2)
            {
                r1.add(boost::bind(&DecisionIrrigation::pSolRetour2, this));
                r2.add(boost::bind(&DecisionIrrigation::pSolRetour2, this));
            } else
            {
                r1.add(boost::bind(&DecisionIrrigation::pSolRetour, this));
                r2.add(boost::bind(&DecisionIrrigation::pSolRetour, this));
            }
            NotRdDArret999(r1);
            NotRdDArret999bis(r2);
        }
    }

    void DecisionIrrigation::NotRdDArret999(ve::Rule& r1) const
    {
        // to manage the exclusive aspect of RdDArret999 with RdDRetour999
        if (isRenseignee(parametres.DateArret)) {
            r1.add(boost::bind(&DecisionIrrigation::pNotDateIrrigArret, this));
            if (parametres.qCondArret == 1) {
                r1.add(boost::bind(&DecisionIrrigation::pNotSomPluieSomETPIrrigArret, this));
            } else {
                if (parametres.IndicateurSolArret == 1) {
                    r1.add(boost::bind(&DecisionIrrigation::pNotFrutIrrigArret, this));
                } else if (parametres.IndicateurSolArret == 2) {
                    r1.add(boost::bind(&DecisionIrrigation::pNotDeficitSatEauIrrigArret, this));
                } else {
                    r1.add(boost::bind(&DecisionIrrigation::pNotDeficitSatPmaxIrrigArret, this));
                }
            }
        }

        if (isRenseignee(parametres.SommeTempArret)) {
            r1.add(boost::bind(&DecisionIrrigation::pNotSomTempIrrigArret, this));
            if (parametres.qCondArret == 1) {
                r1.add(boost::bind(&DecisionIrrigation::pNotSomPluieSomETPIrrigArret, this));
            } else {
                if (parametres.IndicateurSolArret == 1) {
                    r1.add(boost::bind(&DecisionIrrigation::pNotFrutIrrigArret, this));
                } else if (parametres.IndicateurSolArret == 2) {
                    r1.add(boost::bind(&DecisionIrrigation::pNotDeficitSatEauIrrigArret, this));
                } else {
                    r1.add(boost::bind(&DecisionIrrigation::pNotDeficitSatPmaxIrrigArret, this));
                }
            }
        }
    }

    void DecisionIrrigation::NotRdDArret999bis(ve::Rule& r1) const
    {
        // to manage the exclusive aspect of RdDArret999 with RdDRetour999
        if (isRenseignee(parametres.DateArret)) {
            r1.add(boost::bind(&DecisionIrrigation::pNotDateIrrigArret, this));
        }

        if (isRenseignee(parametres.SommeTempArret)) {
            r1.add(boost::bind(&DecisionIrrigation::pNotSomTempIrrigArret, this));
        }
    }

    void DecisionIrrigation::RdDSemisArret999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("ArretIrrigationSemis");
        r1.add(boost::bind(&DecisionIrrigation::pSommePluieIrrigSemis, this));
    }

    // void DecisionIrrigation::RdDSemisDebut999(ve::Activity& a) const
    // {
    //     if (isRenseignee(parametres.nbJoursSemis)) {
    //         ve::Rule& r1 = a.addRule("JoursSemis");
    //         r1.add(boost::bind(&DecisionIrrigation::pNbJoursIrrigSemis, this));
    //         if (parametres.qCondSemis == 1) {
    //             r1.add(boost::bind(&DecisionIrrigation::pSomPluieIrrigSemis, this));
    //         } else {
    //             if(parametres.IndicateurSolSemis == 1) {
    //                 r1.add(boost::bind(&DecisionIrrigation::pFrutIrrigSemis, this));
    //             } else if(parametres.IndicateurSolSemis==2) {
    //                 r1.add(boost::bind(&DecisionIrrigation::pDeficitSatEauIrrigSemis, this));
    //             } else {
    //                 r1.add(boost::bind(&DecisionIrrigation::pDeficitSatPmaxIrrigSemis, this));
    //             }
    //         }
    //     }

    //     if (isRenseignee(parametres.SommeTempSemis)) {
    //         ve::Rule& r2 = a.addRule("SommeTempSemis");
    //         r2.add(boost::bind(&DecisionIrrigation::pSomTempIrrigSemis, this));
    //         if (parametres.qCondSemis == 1) {
    //             r2.add(boost::bind(&DecisionIrrigation::pSomPluieIrrigSemis, this));
    //         } else {
    //             if(parametres.IndicateurSolSemis == 1) {
    //                 r2.add(boost::bind(&DecisionIrrigation::pFrutIrrigSemis, this));
    //             } else if(parametres.IndicateurSolSemis==2) {
    //                 r2.add(boost::bind(&DecisionIrrigation::pDeficitSatEauIrrigSemis, this));
    //             } else{
    //                 r2.add(boost::bind(&DecisionIrrigation::pDeficitSatPmaxIrrigSemis, this));
    //             }
    //         }
    //     }
    // }

    void DecisionIrrigation::RdDSemisDebut_1_999(ve::Activity& a) const
    {
        if (isRenseignee(parametres.nbJoursSemis)) {
            ve::Rule& r1 = a.addRule("JoursSemis");
            r1.add(boost::bind(&DecisionIrrigation::pNbJoursIrrigSemis, this));
        }

        if (isRenseignee(parametres.SommeTempSemis)) {
            ve::Rule& r2 = a.addRule("SommeTempSemis");
            r2.add(boost::bind(&DecisionIrrigation::pSomTempIrrigSemis, this));
        }
    }

    void DecisionIrrigation::RdDSemisDebut_2_999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("IrrSemis");
        r1.add(boost::bind(&DecisionIrrigation::pDateIrrigSemis, this));
        if (parametres.qCondSemis == 1) {
            r1.add(boost::bind(&DecisionIrrigation::pSomPluieIrrigSemis, this));
        } else {
            if(parametres.IndicateurSolSemis == 1) {
                r1.add(boost::bind(&DecisionIrrigation::pFrutIrrigSemis, this));
            } else if(parametres.IndicateurSolSemis==2) {
                r1.add(boost::bind(&DecisionIrrigation::pDeficitSatEauIrrigSemis, this));
            } else {
                r1.add(boost::bind(&DecisionIrrigation::pDeficitSatPmaxIrrigSemis, this));
            }
        }
    }


    void DecisionIrrigation::RdDArretIrrigArret999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("ArretIrrigationArret");
        r1.add(boost::bind(&DecisionIrrigation::pArretIrrigArret, this));
    }

    void DecisionIrrigation::RdDArret999(ve::Activity& a) const
    {
        if (isRenseignee(parametres.DateArret)) {
            ve::Rule& r1 = a.addRule("JoursArret");
            r1.add(boost::bind(&DecisionIrrigation::pDateIrrigArret, this));
            if (parametres.qCondArret == 1) {
                r1.add(boost::bind(&DecisionIrrigation::pSomPluieSomETPIrrigArret, this));
            } else {
                if (parametres.IndicateurSolArret == 1) {
                    r1.add(boost::bind(&DecisionIrrigation::pFrutIrrigArret, this));
                } else if (parametres.IndicateurSolArret == 2) {
                    r1.add(boost::bind(&DecisionIrrigation::pDeficitSatEauIrrigArret, this));
                } else {
                    r1.add(boost::bind(&DecisionIrrigation::pDeficitSatPmaxIrrigArret, this));
                }
            }
        }

        if (isRenseignee(parametres.SommeTempArret)) {
            ve::Rule& r2 = a.addRule("SommeTempArret");
            r2.add(boost::bind(&DecisionIrrigation::pSomTempIrrigArret, this));
            if (parametres.qCondArret == 1) {
                r2.add(boost::bind(&DecisionIrrigation::pSomPluieSomETPIrrigArret, this));
            } else {
                if (parametres.IndicateurSolArret == 1) {
                    r2.add(boost::bind(&DecisionIrrigation::pFrutIrrigArret, this));
                } else if (parametres.IndicateurSolArret == 2) {
                    r2.add(boost::bind(&DecisionIrrigation::pDeficitSatEauIrrigArret, this));
                } else {
                    r2.add(boost::bind(&DecisionIrrigation::pDeficitSatPmaxIrrigArret, this));
                }
            }
        }
    }

    void DecisionIrrigation::RdDRetardPluie999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("RetardPluie");
        r1.add(boost::bind(&DecisionIrrigation::pPluieRetard, this));
    }

    void DecisionIrrigation::RdDRetardVent999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("pRetardVent");
        r1.add(boost::bind(&DecisionIrrigation::pVentRetard, this));
    }

    void DecisionIrrigation::RdDRetardRetour999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("RetardRetour");
        r1.add(boost::bind(&DecisionIrrigation::pRetourRetard, this));
    }

    //
    // Functions
    //

    double DecisionIrrigation::calcFrut() const
    {
        double frut = parametres.ValeursFrutDebut[0];
        for(int i=0;i<3;i++) {
            if(parametres.ValeursFrutDebut[2 * (i + 1)] > faits.StSemis)
                frut = parametres.ValeursFrutDebut[2 * (i + 1) - 1];
            break;
        }
        return frut;
    }

    void DecisionIrrigation::choixPeriodicite(int& PeriodiciteBasse,
                                              int& PeriodiciteMoyenne,
                                              int& PeriodiciteHaute,
                                              double& valBasse,
                                              double& valHaute,
                                              int& valJour) const
    {
        if(faits.STADE < FINCROISFEUIL_SORTIESOIES) {
            PeriodiciteBasse=(int)parametres.Tab1_1[0];
            PeriodiciteMoyenne=(int)parametres.Tab1_3[0];
            PeriodiciteHaute=(int)parametres.Tab1_5[0];
            valBasse=parametres.Tab1_2[0];
            valHaute=parametres.Tab1_4[0];
            valJour=(int)parametres.Tab1_6[0];
        } else if(faits.STADE < AVORTGRAIN_DBTSEN) {
            PeriodiciteBasse=(int)parametres.Tab1_1[1];
            PeriodiciteMoyenne=(int)parametres.Tab1_3[1];
            PeriodiciteHaute=(int)parametres.Tab1_5[1];
            valBasse=parametres.Tab1_2[1];
            valHaute=parametres.Tab1_4[1];
            valJour=(int)parametres.Tab1_6[1];
        } else if(faits.STADE < DBTSEN_PATEUXMOUX) {
            PeriodiciteBasse=(int)parametres.Tab1_1[2];
            PeriodiciteMoyenne=(int)parametres.Tab1_3[2];
            PeriodiciteHaute=(int)parametres.Tab1_5[2];
            valBasse=parametres.Tab1_2[2];
            valHaute=parametres.Tab1_4[2];
            valJour=(int)parametres.Tab1_6[2];
        } else {
            PeriodiciteBasse=(int)parametres.Tab1_1[3];
            PeriodiciteMoyenne=(int)parametres.Tab1_3[3];
            PeriodiciteHaute=(int)parametres.Tab1_5[3];
            valBasse=parametres.Tab1_2[3];
            valHaute=parametres.Tab1_4[3];
            valJour=(int)parametres.Tab1_6[3];
        }
    }

    double DecisionIrrigation::clcDeltaPeriodicite() const
    {
	double n = 0;

	if(faits.STADE < FINCROISFEUIL_SORTIESOIES) {
            if(faits.VolumeRestant > 120) {
                n = parametres.Tab2_5[0];
            } else if(faits.VolumeRestant > 90) {
                n = parametres.Tab2_4[0];
            } else if(faits.VolumeRestant > 60) {
                n = parametres.Tab2_3[0];
            } else if(faits.VolumeRestant > 30) {
                n = parametres.Tab2_2[0];
            } else {
                n = parametres.Tab2_1[0];
            }
	} else if(faits.STADE < AVORTGRAIN_DBTSEN) {
            if(faits.VolumeRestant > 120) {
                n = parametres.Tab2_5[1];
            } else if(faits.VolumeRestant > 90) {
                n = parametres.Tab2_4[1];
            } else if(faits.VolumeRestant > 60) {
                n = parametres.Tab2_3[1];
            } else if(faits.VolumeRestant > 30) {
                n = parametres.Tab2_2[1];
            } else {
                n = parametres.Tab2_1[1];
            }

	} else if(faits.STADE < DBTSEN_PATEUXMOUX) {
            if(faits.VolumeRestant > 120) {
                n = parametres.Tab2_5[2];
            } else if(faits.VolumeRestant > 90) {
                n = parametres.Tab2_4[2];
            } else if(faits.VolumeRestant > 60) {
                n = parametres.Tab2_3[2];
            } else if(faits.VolumeRestant > 30) {
                n = parametres.Tab2_2[2];
            } else {
                n = parametres.Tab2_1[2];
            }

	} else {
            if(faits.VolumeRestant > 120) {
                n = parametres.Tab2_5[3];
            } else if(faits.VolumeRestant > 90) {
                n = parametres.Tab2_4[3];
            } else if(faits.VolumeRestant > 60) {
                n = parametres.Tab2_3[3];
            } else if(faits.VolumeRestant > 30){
                n = parametres.Tab2_2[3];
            } else {
                n = parametres.Tab2_1[3];
            }
	}
	return n;
    }

    double DecisionIrrigation::choixValeur() const
    {
        double valeur = 0;
        if(parametres.isModulation) {
            valeur = clcDeltaPeriodicite();
        } else {
            if(faits.STADE < FINCROISFEUIL_SORTIESOIES) {
                valeur = parametres.Tab1_1[0];
            } else if(faits.STADE < AVORTGRAIN_DBTSEN) {
                valeur = parametres.Tab1_1[1];
            } else if(faits.STADE < DBTSEN_PATEUXMOUX) {
                valeur = parametres.Tab1_1[2];
            } else  {
                valeur = parametres.Tab1_1[3];
            }
        }
        return valeur;
    }
} // namespace Moderato


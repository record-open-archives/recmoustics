/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_PARAMETRESIRRIGATION_HPP
#define MODERATO_PARAMETRESIRRIGATION_HPP

#include <string>
#include <boost/algorithm/string/split.hpp>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include "Utils.hpp"

#include <boost/program_options.hpp>

using namespace boost::algorithm;
namespace vu = vle::utils;
namespace vd = vle::devs;

namespace Moderato {
/**
 * @brief Parameters and values of the irrigation
 *
 * This object is a simple structure containing all the parameters,
 * and their values used by the irrigation rules,
 * and the irrigation operation.
 */
class ParametresIrrigation
{
public:
    ParametresIrrigation() { };

    virtual ~ParametresIrrigation() { };

    /**
     * @brief Setting of the parameters
     *
     * @param paramfile is the complete path to the config file
     * @param time is an access to the year of the simulation
     */
    void init(const std::string paramfile, const vd::Time& time);

    //RdD Irrigation Semis
    bool isRdDSemis;
    int nbJoursSemis;
    double SommeTempSemis;
    int qCondSemis;
    int IndicateurSolSemis;
    double SommePluieSemis;
    double FrutSemis;
    int qDoseSemis;
    int IndicateurDoseSemis;
    double DoseSemis;
    double ArretSemis;
    bool isARetirerSemis;

    //RdD Début Irrigation
    vd::Time DateDebut;
    double SommeTempDebut;
    bool isOU_ET;
    double SommeTempDebutMax;
    int qCondDebut;
    int IndicateurSolDebut;
    double ValeursPluieDebut[3];
    double ValeursFrutDebut[7];
    int qDoseDebut;
    int IndicateurDoseDebut;
    double DoseDebut;

    //RdD Retour Irrigation
    int qCondRetour;
    int qCond2Retour;
    int IndicateurSolRetour;
    int qDoseRetour;
    int IndicateurDoseRetour;
    bool isModulation;
    bool isF1egF21;
    bool isF24egF3;
    double Tab1_1[4];
    double Tab1_2[4];
    double Tab1_3[4];
    double Tab1_4[4];
    double Tab1_5[4];
    double Tab1_6[4];
    double Tab1_7[4];
    double Tab2_1[4];
    double Tab2_2[4];
    double Tab2_3[4];
    double Tab2_4[4];
    double Tab2_5[4];

    //RdD Retard
    bool isRdDRetard;
    bool isCondPluieRetard;
    double ValeursPluie[5];
    bool isDecouple;
    bool isRetour1Retard;
    double Retour1Retard;
    bool isCondVentRetard;
    double ValeursVent[2];

    //RdD Arret
    vd::Time DateArret;
    double SommeTempArret;
    int qCondArret;
    int IndicateurSolArret;
    double ValeursClimatArret[7];
    double FrutArret;
    int qDoseArret;
    int IndicateurDoseArret;
    double DoseArret;
    double ArretArret;

    //RdD Irrigation Anticipation
    bool isRdDAnticipation;
    double ValeurPluieAnticipation;
    int nbJoursPluieAnticipation;
    int nbJoursReportAnticipation;
};

} // namespace Moderato

#endif


/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_PARAMETRESRECOLTE_HPP
#define MODERATO_PARAMETRESRECOLTE_HPP

#include <string>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include "Utils.hpp"

#include <boost/program_options.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;

namespace Moderato {
/**
 * @brief Parameters and values of the harvesting
 *
 * This object is a simple structure containing all the parameters,
 * and their values used by the harvesting rule,
 * and the harvesting operation.
 */
class ParametresRecolte
{
public:

    ParametresRecolte() :
        DateLimiteRecolte(0.0), SommeTemperature(0.0), HumiditeGrain(0.0),
        SommePluie(0.0), nbJours(0), Frut(0.0), IndicateurSol(0)
    { };

    virtual ~ParametresRecolte() { };

    /**
     * @brief Setting of the parameters
     *
     * @param paramfile is the complete path to the config file
     * @param time is an access to the year of the simulation
     */
    void init(const std::string paramfile, const vd::Time& time);

    vd::Time DateLimiteRecolte;
    double SommeTemperature;
    double HumiditeGrain;
    double SommePluie;
    int nbJours;
    double Frut;
    int IndicateurSol;

};

} // namespace Moderato

#endif


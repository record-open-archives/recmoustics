/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "DecisionSemis.hpp"

namespace Moderato {

    void DecisionSemis::init(const std::string paramfile,
                             const vd::Time& time)
    {
        parametres.init(paramfile, time);
    }

    bool DecisionSemis::dateLimiteSemis() const
    {
        const ve::Activity& a = agent.activities().get("Semis")->second;
        return agent.currentTime() == a.finish();
    }

    bool DecisionSemis::portanceMeteo() const
    {
        return accumulate(faits.Pluies.begin(), faits.Pluies.begin() + parametres.nbJours - 1, 0.) < parametres.SommePluie;
    }

    bool DecisionSemis::portanceSol1() const
    {
        return parametres.Frut >= faits.Frut;
    }

    bool DecisionSemis::portanceSol2() const
    {
        return parametres.Frut <= faits.DeficitSaturationEau;
    }

    bool DecisionSemis::portanceSol() const
    {
        return parametres.Frut <= faits.DeficitSaturationPmax;
    }

    void DecisionSemis::RdDDebut999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("conditionSemis");

        if(isRenseignee(parametres.SommePluie)) {
            r1.add(boost::bind(&DecisionSemis::portanceMeteo, this));
        }  else if (isRenseignee(parametres.Frut)) {
            if( parametres.IndicateurSol == 1) {
                r1.add(boost::bind(&DecisionSemis::portanceSol1, this));
            } else if (parametres.IndicateurSol == 2) {
                r1.add(boost::bind(&DecisionSemis::portanceSol2, this));
            } else {
                r1.add(boost::bind(&DecisionSemis::portanceSol, this));
            }
        }

        ve::Rule& r2 = a.addRule("dateLimiteSemis");
        r2.add(boost::bind(&DecisionSemis::dateLimiteSemis, this));
    }
} // namespace Moderato


/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/devs/Dynamics.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>
#include <vle/vpz.hpp>
#include <boost/numeric/conversion/cast.hpp>

#include "Utils.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
namespace vz = vle::vpz;

namespace Moderato {
/**
 * @brief a Output observation vle::devs::Dynamics model
 *
 * A very simple model to provide outputs at the end of the simulation
 *
 * the list of ports :
 * - Experimental condition :
 * - Input :
 *  - STADE :
 *  - MSG :
 *  - DateSemis :
 *  - DateRecolte :
 *  - SETP
 *  - SRain
 * - Output :
 * - Observation :
 *  - Rendement :
 *  - DateSemis :
 *  - DateRecolte :
 *
 * @todo move dateOnly to Utils.hpp
 */
class ObservatorOutput: public vd::Dynamics
{

public:
    ObservatorOutput(const vd::DynamicsInit& mdl,
                     const vd::InitEventList& evts)
        : vd::Dynamics(mdl, evts),
          DateSemis(vd::Time::infinity),
          DateRecolte(vd::Time::infinity)
    {
    };

    virtual ~ObservatorOutput() { }

    virtual vd::Time init(const vd::Time& /*time*/)
    {
        return vd::Time::infinity;
    };

    vd::Time timeAdvance() const
    {
        return vd::Time::infinity;
    };

    /**
     * @brief When receiving an event, use it to provide information
     * needed at the end of the simulation.
     *
     * @param events the received events
     * @param time the current time
     */
    void externalTransition(
        const vd::ExternalEventList& events,
        const vd::Time& time)
    {
        for (vd::ExternalEventList::const_iterator it = events.begin();
             it != events.end(); ++it) {

            if ((*it)->onPort("DateSemis")) {
                DateSemis = time;
            } else if ((*it)->onPort("DateRecolte")) {
                DateRecolte = time;
            } else if ((*it)->onPort("MSG")) {
                if (time == DateRecolte) {
                    MSG = (*it)->getDoubleAttributeValue("value");
                }
            }
        }
    };

    vv::Value* observation(const vd::ObservationEvent& event) const
    {
        if (event.onPort("Rendement")) {
            return buildDouble(MSG);
        }
        if (event.onPort("DateSemis")) {
            return buildString(dateOnly(DateSemis));
        }
        if (event.onPort("DateRecolte")) {
            return buildString(dateOnly(DateRecolte));
        }
        return 0;
    }

    /*
     * @brief get the date only(without the time)
     *
     */
    std::string dateOnly(const vd::Time& time) const
    {
        std::string date = vu::DateTime::toJulianDay(time);

        return date.substr(0, date.find(":"));
    }

protected:

    // Variables D'état
    double MSG;
    vd::Time DateSemis;
    vd::Time DateRecolte;
};

} // namespace Moderato

DECLARE_DYNAMICS(Moderato::ObservatorOutput)

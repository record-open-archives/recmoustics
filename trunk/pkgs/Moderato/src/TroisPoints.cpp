/**
 * @file
 * @author The RECORD Development Team (INRA)
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 *
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>
#include "ParametresContexte.hpp"
#include "ParametresBase.hpp"

using namespace std;
using namespace boost;

namespace vu = vle::utils;
namespace vv = vle::value;

namespace Moderato {
/*
 * \todo{la doc}
 */
class TroisPoints : public vle::extension::DifferenceEquation::Multiple
{
public:
    TroisPoints(const vle::devs::DynamicsInit& init,
                const vle::devs::InitEventList& events) :
	vle::extension::DifferenceEquation::Multiple(init, events)
    {
        // gestion des conditions expérimentales
        // qui permettent de charger les paramètres.

        string nomFichier = toString(events.get("nomFichierContexte"));
        filepathContexte = vu::Path::path().getPackageDataFile(nomFichier);

        nomFichier = toString(events.get("nomFichierBase"));
        filepathBase = vu::Path::path().getPackageDataFile(nomFichier);

        Lai = createVar("Lai");
        STADE = createVar("STADE");
        StSemis = createVar("StSemis");
        HumiditeGrain = createVar("HumiditeGrain");
        Frut = createVar("Frut");
        DeficitSaturationEau = createVar("DeficitSaturationEau");
        DeficitSaturationPmax = createVar("DeficitSaturationPmax");

        Lai_1 = createSync("Lai_1");
        STADE_1 = createSync("STADE_1");
        StSemis_1 = createSync("StSemis_1");
        HumiditeGrain_1 = createSync("HumiditeGrain_1");
        Frut_1 = createSync("Frut_1");
        DeficitSaturationEau_1 = createSync("DeficitSaturationEau_1");
        DeficitSaturationPmax_1 = createSync("DeficitSaturationPmax_1");

        Lai_2 = createSync("Lai_2");
        STADE_2 = createSync("STADE_2");
        StSemis_2 = createSync("StSemis_2");
        HumiditeGrain_2 = createSync("HumiditeGrain_2");
        Frut_1 = createSync("Frut_2");
        DeficitSaturationEau_1 = createSync("DeficitSaturationEau_2");
        DeficitSaturationPmax_1 = createSync("DeficitSaturationPmax_2");

        Lai_3 = createSync("Lai_3");
        STADE_3 = createSync("STADE_3");
        StSemis_3 = createSync("StSemis_3");
        HumiditeGrain_3 = createSync("HumiditeGrain_3");
        Frut_3 = createSync("Frut_3");
        DeficitSaturationEau_3 = createSync("DeficitSaturationEau_3");
        DeficitSaturationPmax_3 = createSync("DeficitSaturationPmax_3");
    }

    virtual ~TroisPoints() { }

    virtual void compute(const vle::devs::Time& /* time */)
    {
        if(parametresBase.nbPositions == 1) {
            Lai = Lai_1();
            STADE = STADE_1();
            StSemis = StSemis_1();
            HumiditeGrain = HumiditeGrain_1();
            Frut = Frut_1();
            DeficitSaturationEau = DeficitSaturationEau_1();
            DeficitSaturationPmax = DeficitSaturationPmax_1();
        } else { //parametresBase.nbPositions == 3
            switch ( parametresContexte.Position) {
            case 1 :
                Lai = Lai_1();
                STADE = STADE_1();
                StSemis = StSemis_1();
                HumiditeGrain = HumiditeGrain_1();
                Frut = Frut_1();
                DeficitSaturationEau = DeficitSaturationEau_1();
                DeficitSaturationPmax = DeficitSaturationPmax_1();
                break;
            case 2 :
                Lai = Lai_2();
                STADE = STADE_2();
                StSemis = StSemis_2();
                HumiditeGrain = HumiditeGrain_2();
                Frut = Frut_2();
                DeficitSaturationEau = DeficitSaturationEau_2();
                DeficitSaturationPmax = DeficitSaturationPmax_2();
                break;
            case 3 :
                Lai = Lai_3();
                STADE = STADE_3();
                StSemis = StSemis_3();
                HumiditeGrain = HumiditeGrain_3();
                Frut = Frut_3();
                DeficitSaturationEau = DeficitSaturationEau_3();
                DeficitSaturationPmax = DeficitSaturationPmax_3();
                break;
            case 4 :
                Lai =  (Lai_1() +  Lai_2() +  Lai_3()) / 3;
                STADE = (STADE_1() + STADE_2() + STADE_3()) / 3;
                StSemis = (StSemis_1() + StSemis_2() +  StSemis_3()) / 3;
                HumiditeGrain = (HumiditeGrain_1() + HumiditeGrain_2() + HumiditeGrain_3())/3;
                Frut = (Frut_1() + Frut_2() + Frut_3()) / 3;
                DeficitSaturationEau = (DeficitSaturationEau_1() + DeficitSaturationEau_2() + DeficitSaturationEau_3()) / 3;
                DeficitSaturationPmax = (DeficitSaturationPmax_1() + DeficitSaturationPmax_2() + DeficitSaturationPmax_3()) / 3;
                break;
            }
        }
    }

    virtual void initValue(const vle::devs::Time& time)
    {
        parametresContexte.init(filepathContexte,time);
        parametresBase.init(filepathBase);

        Lai = 0.0;
        STADE = 0.0;
        StSemis = 0.0;
        HumiditeGrain = 0.0;
        Frut = 0.0;
        DeficitSaturationEau = 0.0;
        DeficitSaturationPmax = 0.0;
    }

private:

    Var Lai;
    Var STADE;
    Var StSemis;
    Var HumiditeGrain;
    Var Frut;
    Var DeficitSaturationEau;
    Var DeficitSaturationPmax;

    Sync Lai_1;
    Sync STADE_1;
    Sync StSemis_1;
    Sync HumiditeGrain_1;
    Sync Frut_1;
    Sync DeficitSaturationEau_1;
    Sync DeficitSaturationPmax_1;

    Sync Lai_2;
    Sync STADE_2;
    Sync StSemis_2;
    Sync HumiditeGrain_2;
    Sync Frut_2;
    Sync DeficitSaturationEau_2;
    Sync DeficitSaturationPmax_2;

    Sync Lai_3;
    Sync STADE_3;
    Sync StSemis_3;
    Sync HumiditeGrain_3;
    Sync Frut_3;
    Sync DeficitSaturationEau_3;
    Sync DeficitSaturationPmax_3;

    string filepathContexte;
    string filepathBase;

    ParametresContexte parametresContexte;
    ParametresBase parametresBase;
};

} //namespace Moderato

DECLARE_DYNAMICS(Moderato::TroisPoints)

/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "Utils.hpp"

namespace Moderato {

    vd::Time toTime(const std::string paramTime,
                    const vd::Time& time)
    {
        std::string year = lexical_cast<std::string>(vu::DateTime::year(time));

        typedef std::vector< std::string > split_vector_type;
        split_vector_type SplitVec;

        if (isRenseignee(paramTime)) {
            boost::split(SplitVec, paramTime, boost::is_any_of("/") );
            return (double)vu::DateTime::toJulianDayNumber(
                year + "-" + SplitVec[1] + "-" + SplitVec[0]);
        } else {
            return -999.;
        }
    }

    bool isRenseignee(const double val)
    {
        return val != -999.;
    }

    bool isRenseignee(const int val)
    {
        return val != -999;
    }

    bool isRenseignee(const std::string val)
    {
        return val != "-999";
    }

} // namespace Moderato

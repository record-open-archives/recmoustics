/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/devs/DynamicsDbg.hpp>
#include <vle/utils/i18n.hpp>

#include "ParametresSemis.hpp"

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension;
namespace va = vle::extension::fsa;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

namespace Moderato
{
/**
 * @brief a sowing operation vle::extension::fsa::Statechart model
 *
 * A very simple sowing model based on
 * the statechart formalism.
 * As soon as the model receive an event(order) on the port
 * "Start"; it sends an event with a value named value.
 * The value is the density of fertilization wanted, and is a
 * parameter.
 * The name of the value is "Semis".
 * Sowing can be processed only once.
 *
 * The experimental condition expected is the config file name,
 * in order to load the parameters.
 *
 * the list of ports :
 * - Experimental condition :
 *  - nomFichierSemis [String] relative file path of the config file
 * - Input :
 *  - Start receiving an empty event.
 * - Output :
 *  - ack : sending an event with 2 attributes :
 *   - name [String] "Semis"
 *   - value [String] "done"
 *  - Fertilisation : sending a "Semis" Difference equation
 * value.
 * - Observation :
 *  - State
 */
class ASemis: public va::Statechart
{
public:

enum State {BEFORE, AFTER};

ASemis(const vd::DynamicsInit& atom,
       const vd::InitEventList& events)
    : va::Statechart(atom, events)
    {
        // parameter managment

        string nomFichierSemis = toString(events.get("nomFichierSemis"));
        filepath = vu::Path::path().
            getPackageDataFile(nomFichierSemis);

        // The structure of the statechart

        states(this) << BEFORE << AFTER;

        transition(this, BEFORE, AFTER)
            << event("Start")
            << send(&ASemis::out);

        state(this, BEFORE) << inAction(&ASemis::Init);

        initialState(BEFORE);
    }

    virtual ~ASemis()
    {
    }

private:
    void Init(const vd::Time& time)
    {
        // section Paramètres999
        string section = "Paramètres999";
        try
        {
            parametres.init(filepath, time);
        } catch(std::exception& e) {
            throw vu::ModellingError(vle::fmt(_("[%1%] %2% \
while loading parameters                                   \
from file %3% and section %4%")) %
                                     getModelName() %
                                     e.what() %
                                     filepath %
                                     section);
        }
    }

    /**
     * @brief The operation correspond to the send of an event to the
     * biophysical model.
     *
     * The operation need to send an acknolegment to the Decision
     * Agent, in order to mention that the operation is done.
     *
     * @param output is an access to the ports.
     */
    void out(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
    {
        double Densite = parametres.Densite;

        output << (ve::DifferenceEquation::Var("Semis") = Densite);

        vd::ExternalEvent* order = new vd::ExternalEvent("ack");
        order->putAttribute("name", new vv::String("Semis"));
        order->putAttribute("value", new vv::String("done"));
        output.addEvent(order);
    }

    ParametresSemis parametres; //!< access to the parameters

    string filepath;
};

} //namespace Moderato

DECLARE_DYNAMICS_DBG(Moderato::ASemis)

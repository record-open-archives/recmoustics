/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "DecisionRecolte.hpp"

namespace Moderato {

    void DecisionRecolte::init(const std::string paramfile,
                               const vd::Time& time)
    {
        parametres.init(paramfile, time);
    }

    bool DecisionRecolte::dateLimiteRecolte() const
    {
        const ve::Activity& a = agent.activities().get("Recolte")->second;
        return agent.currentTime() == a.finish();
    }

    bool DecisionRecolte::sommeTemperature() const
    {
        return faits.StSemis >= parametres.SommeTemperature;
    }

    bool DecisionRecolte::humiditeGrain() const
    {
        return faits.HumiditeGrain <= parametres.HumiditeGrain;
    }

    bool DecisionRecolte::portanceMeteo() const
    {
        return accumulate(faits.Pluies.begin(),
                          faits.Pluies.begin() + parametres.nbJours - 1,
                          0.) < parametres.SommePluie;
    }

    bool DecisionRecolte::portanceSol1() const
    {
        return parametres.Frut >= faits.Frut;
    }

    bool DecisionRecolte::portanceSol2() const
    {
        return parametres.Frut <= faits.DeficitSaturationEau;
    }

    bool DecisionRecolte::portanceSol() const
    {
        return parametres.Frut <= faits.DeficitSaturationPmax;
    }

    void DecisionRecolte::RdDDebut999(ve::Activity& a) const
    {
        ve::Rule& r1 = a.addRule("sommeTemperature");
        ve::Rule& r2 = a.addRule("humiditeGrain");

        if (isRenseignee(parametres.SommeTemperature)) {
            r1.add(boost::bind(&DecisionRecolte::sommeTemperature, this));
        }

        if (isRenseignee(parametres.HumiditeGrain)) {
            r2.add(boost::bind(&DecisionRecolte::humiditeGrain, this));
        }
        if(isRenseignee(parametres.SommePluie)) {
            r1.add(boost::bind(&DecisionRecolte::portanceMeteo, this));
            r2.add(boost::bind(&DecisionRecolte::portanceMeteo, this));
        }  else if (isRenseignee(parametres.Frut)) {
            if( parametres.IndicateurSol == 1) {
                r1.add(boost::bind(&DecisionRecolte::portanceSol1, this));
                r2.add(boost::bind(&DecisionRecolte::portanceSol1, this));
            } else if (parametres.IndicateurSol == 2) {
                r1.add(boost::bind(&DecisionRecolte::portanceSol2, this));
                r2.add(boost::bind(&DecisionRecolte::portanceSol2, this));
            } else {
                r1.add(boost::bind(&DecisionRecolte::portanceSol, this));
                r2.add(boost::bind(&DecisionRecolte::portanceSol, this));
            }
        }

        ve::Rule& r3 = a.addRule("dateLimiteRecolte");
        r3.add(boost::bind(&DecisionRecolte::dateLimiteRecolte, this));
    }
} // namespace Moderato

/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "Faits.hpp"

namespace Moderato {

    void Faits::stade(const vv::Value& value)
    {
        STADE = (STADEPHYSIO)value.toDouble().value();
    }

    void Faits::pluie(const vv::Value& value)
    {
        rotate(Pluies.rbegin(), Pluies.rbegin() + 1,
               Pluies.rend());
        Pluies[0] = value.toDouble().value();

        rotate(PluiesRetard.rbegin(), PluiesRetard.rbegin() + 1,
               PluiesRetard.rend());
        PluiesRetard[0] = value.toDouble().value();
    }

    void Faits::resetretardpluie(const vv::Value& value)
    {
        PluiesRetard.assign (10, 0.);
        PluiesRetard[0] = value.toDouble().value();
    }

    double Faits::getretardpluie(double efficacite, int duree) const
    {
        Filtre filtre(efficacite);

        return accumulate(PluiesRetard.begin(),
                          PluiesRetard.begin() + duree - 1,
                          0., filtre);
    }

    double Faits::getnbjoursretard(double efficacite, int duree,
                                   double ratio) const
    {
        return std::floor((getretardpluie(efficacite, duree) / ratio) + 0.5);
    }

    int Faits::getnbjoursapressemis(double date) const
    {
        return (int) date - dateSemis;
    }

    void Faits::etp(const vv::Value& value)
    {
        rotate(ETP.rbegin(), ETP.rbegin() + 1,
               ETP.rend());
        ETP[0] = value.toDouble().value();
    }

    void Faits::stsemis(const vv::Value& value)
    {
        StSemis = value.toDouble().value();
    }

    void Faits::humiditegrain(const vv::Value& value)
    {
        HumiditeGrain = value.toDouble().value();
    }

    void Faits::frut(const vv::Value& value)
    {
        Frut = value.toDouble().value();
    }

    void Faits::deficitsaturationeau(const vv::Value& value)
    {
        DeficitSaturationEau = value.toDouble().value();
    }

    void Faits::deficitsaturationpmax(const vv::Value& value)
    {
        DeficitSaturationPmax = value.toDouble().value();
    }

    void Faits::nbjourseffectifs(const vv::Value& value)
    {
        nbJoursEffectifs = value.toDouble().value();
    }

    void Faits::volumerestant(const vv::Value& value)
    {
        VolumeRestant = value.toDouble().value();
    }

    void Faits::nbjoursdepuisdbtirrigation(const vv::Value& value)
    {
        nbJoursDepuisDbtIrrigation = value.toDouble().value();
    }

    void Faits::datesemis(const vv::Value& value)
    {
        dateSemis = value.toDouble().value();
    }

    void Faits::daterecolte(const vv::Value& value)
    {
        dateRecolte = value.toDouble().value();
    }

    void Faits::vent(const vv::Value& value)
    {
        Vent = value.toDouble().value();
    }

    void Faits::semis(const vv::Value& value)
    {
        irrigationSemis = value.toDouble().value();
    }

    void Faits::dateirrigsemis(const vv::Value& value)
    {
        dateIrrigSemis= value.toDouble().value();
    }
} // namespace Moderato


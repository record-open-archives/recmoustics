/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "ParametresFertilisation.hpp"

using namespace std;

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace po = boost::program_options;

namespace Moderato {

    void ParametresFertilisation::init(const std::string paramfile)
    {
        ifstream config(paramfile.c_str());

        if (not config) {
            throw vu::ModellingError(
                vle::fmt(_("ParametresFertilisation - cannot"	\
                           " open file : %1%"))
                % paramfile);
        }

        po::options_description desc("Options");
        desc.add_options()
            ("Parametres999.Quantite", po::value<double>(), "")
            ("Parametres999.Stade", po::value<int>(), "");

        po::variables_map vm;
        po::store(po::parse_config_file(config, desc, true), vm);
        po::notify(vm);

        std::string section = "Parametres999.";

        Quantite =  vm[section + "Quantite"].as<double>();
        STADE =  (STADEPHYSIO)lexical_cast<int>(vm[section + "Stade"].as<int>());
    };

} // namespace Moderato


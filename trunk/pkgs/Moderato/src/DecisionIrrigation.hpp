/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_DECISIONIRRIGATION_HPP
#define MODERATO_DECISIONIRRIGATION_HPP

#include <vle/extension/Decision.hpp>

#include "Faits.hpp"
#include "ParametresIrrigation.hpp"
#include "Utils.hpp"

namespace ve = vle::extension::decision;

namespace Moderato {

/**
 * @brief predicates and decision rules of the irrigation used by Moderato
 *
 * This object is hosting all the predicates necessary to the decision
 * rule. It also contains the parameters and it has an access to the
 * facts the decision depends on.  This object also build the decision
 * by agregating predicates.
 */
class DecisionIrrigation
{
public:

    DecisionIrrigation(const Faits& f, const ve::Agent& a) : faits(f), agent(a)
    { };

    ~DecisionIrrigation() { };

    void init(const std::string paramfile, const vd::Time& time);

    /**
     * @brief predicate
     */
    bool notSommeTemperatureMax() const;
    /**
     * @brief predicate
     */
    bool dateDebutIrrigation() const;
    /**
     * @brief predicate
     */
    bool sommeTemperature() const;
    /**
     * @brief predicate
     */
    bool sommePluie() const;
    /**
     * @brief predicate
     */
    bool sommeETP() const;
    /**
     * @brief predicate
     */
    bool conditionSol1() const;
    /**
     * @brief predicate
     */
    bool conditionSol2() const;
    /**
     * @brief predicate
     */
    bool conditionSol() const;
    /**
     * @brief predicate
     */
    bool periodeModuleeSimple() const;
    /**
     * @brief predicate
     */
    bool periodeModulee() const;
    /**
     * @brief predicate
     */
    bool periodeFixe() const;
    /**
     * @brief predicate
     */
    bool pSolRetour1() const;
    /**
     * @brief predicate
     */
    bool pSolRetour2() const;
    /**
     * @brief predicate
     */
    bool pSolRetour() const;
    /**
     * @brief predicate
     *
     * used to stop the IrrigationSemis activity.
     */
    bool pSommePluieIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pNbJoursIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pSomTempIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pSomPluieIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pFrutIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pDeficitSatEauIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pDeficitSatPmaxIrrigSemis() const;
    /**
     * @brief predicate
     */
    bool pArretIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pDateIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pSomTempIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pSomPluieSomETPIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pFrutIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pDeficitSatEauIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pDeficitSatPmaxIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pNotDateIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pNotSomTempIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pNotSomPluieSomETPIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pNotFrutIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pNotDeficitSatEauIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pNotDeficitSatPmaxIrrigArret() const;
    /**
     * @brief predicate
     */
    bool pPluieRetard() const;
    /**
     * @brief predicate
     */
    bool pVentRetard() const;
    /**
     * @brief predicate
     */
    bool pRetourRetard() const;
    /**
     * @brief predicate ckecking if this is the good date for semis irrigation
     */
    bool pDateIrrigSemis() const;
    /**
     * @brief predicate describing the relationship between the
     * IrrigationSemis Activity and the IrrigationDebutActivity.
     *
     * In other words, when this predicate is true the rule of the
     * IrrigationDebut can be evaluated.
     */
    bool pIrrigationSemisClear() const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDDebut999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDRetour999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDSemisArret999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDSemisDebut_1_999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDSemisDebut_2_999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDArretIrrigArret999(ve::Activity& a)const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDArret999(ve::Activity& a) const;
    /**
     * @brief aggregate predicate to a rule.
     *
     * param the rule.
     */
    void NotRdDArret999(ve::Rule& r1) const;
    /**
     * @brief aggregate predicate to a rule.
     *
     * param the rule.
     */
    void NotRdDArret999bis(ve::Rule& r1) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDRetardPluie999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDRetardVent999(ve::Activity& a) const;
    /**
     * @brief build the rule and add it to the activity.
     *
     * param a the activity the rule is added to.
     */
    void RdDRetardRetour999(ve::Activity& a) const;


    /**
     * @brief
     */
    double calcFrut() const;
    /**
     * @brief
     */
    double clcDeltaPeriodicite() const;
    /**
     * @brief
     */
    void choixPeriodicite(int& PeriodiciteBasse,
                          int& PeriodiciteMoyenne,
                          int& PeriodiciteHaute,
                          double& valBasse,
                          double& valHaute,
                          int& valJour) const;
    /**
     * @brief
     */
    double choixValeur() const;

    ParametresIrrigation parametres; //!< access to the parameters

private:

    const Faits& faits;  //!< access to facts used to take a decision
    const ve::Agent& agent;  //!< access to time of the simulation

};
} // namespace Moderato

#endif

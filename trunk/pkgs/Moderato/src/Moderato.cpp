/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/Decision.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>

#include "Utils.hpp"
#include "DecisionSemis.hpp"
#include "DecisionFertilisation.hpp"
#include "DecisionIrrigation.hpp"
#include "DecisionRecolte.hpp"
#include "Faits.hpp"

using namespace std;

using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ved = vle::extension::decision;
namespace vee = vle::extension::DifferenceEquation;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace Moderato
{
/**
 * @brief a planning vle::extension::decision::Agent model
 *
 * the list of ports :
 * - Experimental condition :
 *  - nomFichierFertilisation [String] relative file path of the
 * config file.
 * - Input (and also facts except for "ack") :
 *  - ack : a common port to receive acknolegment of activities
 *  - STADE : the moderato stage of a maïs crop.
 *  - Pluie : the dayly amount of rain
 *  - StSemis
 *  - HumiditeGrain
 *  - Frut
 *  - DeficitSaturationEau
 *  - DeficitSaturationPmax
 *  - ETP
 *  - nbJoursEffectifs
 *  - VolumeRestant
 *  - nbJoursDepuisDbtIrrigation
 *  - dateSemis
 *  - dateRecolte
 *  - resetRetardPluie
 *  - Vent
 *  - Semis : IrrigationSemis
 *    - 0. = not started
 *    - 1. = in progress
 *    - 2. = done
 *  - dateIrrigSemis =  the date the irrigation semis could start
 * the list of activities :
 * - Fertilisation : this activity is not related to another one, the
 * fertilization happens only once.
 * - Semis : this activity does not depend to another one. The sowing
 * happens only once.
 * - Recolte : this activity depends on Semis. The harvesting happens
 * only once, but as soon it happens, it does stop the Irrigation
 * process by sending a "Stop" order, and also by unplanning already
 * planned activities.
 * happens only once.
 */

class Moderato: public ved::Agent
{

public:

    Moderato(const vd::DynamicsInit& mdl, const vd::InitEventList& events)
        : ved::Agent(mdl, events),
          hasSemis(false), hasFertilisation(false), hasIrrigation(false),
          hasRecolte(false), decisionSemis(BdFaits, *this),
          decisionFertilisation(BdFaits),   decisionIrrigation(BdFaits, *this),
          decisionRecolte(BdFaits, *this), mIrrigationRetourNumber(1),
          mIrrigationRetardNumber(1)
    {
        // gathering the full path of the irrigation config file
        if (events.exist("nomFichierIrrigation")) {
            hasIrrigation = true;
            string nomFichierIrrigation = toString(events.get("nomFichierIrrigation"));
            filepathIrrigation = vu::Path::path().getPackageDataFile(nomFichierIrrigation);
        }

        // gathering the full path of the fertilization config file
        if (events.exist("nomFichierFertilisation")) {

            hasFertilisation = true;
            string nomFichierFertilisation = toString(events.get("nomFichierFertilisation"));

            filepathFertilisation = vu::Path::path().getPackageDataFile(nomFichierFertilisation);
        }

        // gathering the full path of the harvesting config file
        if (events.exist("nomFichierRecolte")) {
            hasRecolte = true;
            string nomFichierRecolte = toString(events.get("nomFichierRecolte"));
            filepathRecolte = vu::Path::path().getPackageDataFile(nomFichierRecolte);
        }

        // gathering the full path of the sowing config file
        if (events.exist("nomFichierSemis")) {
            hasSemis = true;
            string nomFichierSemis = toString(events.get("nomFichierSemis"));
            filepathSemis = vu::Path::path().getPackageDataFile(nomFichierSemis);
        }

        // facts management
        addFact("STADE",
                boost::bind(&Faits::stade, &BdFaits, _1));
        addFact("Pluie",
                boost::bind(&Faits::pluie, &BdFaits, _1));
        addFact("StSemis",
                boost::bind(&Faits::stsemis, &BdFaits, _1));
        addFact("HumiditeGrain",
                boost::bind(&Faits::humiditegrain, &BdFaits, _1));
        addFact("Frut",
                boost::bind(&Faits::frut, &BdFaits, _1));
        addFact("DeficitSaturationEau",
                boost::bind(&Faits::deficitsaturationeau, &BdFaits, _1));
        addFact("DeficitSaturationPmax",
                boost::bind(&Faits::deficitsaturationpmax, &BdFaits, _1));
        addFact("ETP",
                boost::bind(&Faits::etp, &BdFaits, _1));
        addFact("nbJoursEffectifs",
                boost::bind(&Faits::nbjourseffectifs, &BdFaits, _1));
        addFact("VolumeRestant",
                boost::bind(&Faits::volumerestant, &BdFaits, _1));
        addFact("nbJoursDepuisDbtIrrigation",
                boost::bind(&Faits::nbjoursdepuisdbtirrigation, &BdFaits, _1));
        addFact("dateSemis",
                boost::bind(&Faits::datesemis, &BdFaits, _1));
        addFact("dateRecolte",
                boost::bind(&Faits::daterecolte, &BdFaits, _1));
        addFact("resetRetardPluie",
                boost::bind(&Faits::resetretardpluie, &BdFaits, _1));
        addFact("Vent",
                boost::bind(&Faits::vent, &BdFaits, _1));
        addFact("Semis",
                boost::bind(&Faits::semis, &BdFaits, _1));
        addFact("dateIrrigSemis",
                boost::bind(&Faits::dateirrigsemis, &BdFaits, _1));
    }

    /**
     * @brief plan the activities at initial time.
     */
    vd::Time init(const vd::Time& time)
    {
        //
        // planning the single Sowing activity
        //
        if(hasSemis) {
            // Initialisation of the predicate host and rule builder
            try
            {
                decisionSemis.init(filepathSemis,time);

            } catch(exception& e) {
                throw vu::ModellingError(vle::fmt(_("[%1%] %2% while loading parameters \
from file %3%")) % getModelName() % e.what() % filepathSemis);
            }
            // declaration
            ved::Activity& a = addActivity("Semis", decisionSemis.parametres.DateMin,
                                           decisionSemis.parametres.DateMax);
            // binding of the action related to the activity
            a.addOutputFunction(boost::bind(
                                    &Moderato::outSemis, this, _1, _2, _3));
            // connecting the rule to the activity
            decisionSemis.RdDDebut999(a);
        }
        //
        // planning the single Harvesting activity
        //
        if(hasRecolte) {
            // Initialisation of the predicate host and rule builder
            try
            {
                decisionRecolte.init(filepathRecolte, time);
            } catch(exception& e) {
                throw vu::ModellingError(vle::fmt(_("[%1%] %2% while loading parameters \
from file %3%")) % getModelName() % e.what() % filepathRecolte);
            }
            // declaration
            ved::Activity& a = addActivity("Recolte", vd::Time::negativeInfinity,
                                          decisionRecolte.parametres.DateLimiteRecolte);

            // binding of the action related to the activity
            a.addOutputFunction(boost::bind(
                                    &Moderato::outRecolte, this, _1, _2, _3));
            // connecting the rule to the activity
            decisionRecolte.RdDDebut999(a);

            // connecting unplanification task
            a.addAcknowledgeFunction(
                boost::bind(&Moderato::ackRecolte,this, _1, _2));
        }
        //
        // planning the single Fertilisation activity
        //
        if(hasFertilisation) {
            // Initialisation of the predicate host and rule builder
            try
            {
                decisionFertilisation.init(filepathFertilisation);

            } catch(exception& e) {
                throw vu::ModellingError(vle::fmt(_("[%1%] %2% while loading parameters \
from file %3% and section %4%")) % getModelName() % e.what() % filepathFertilisation);
            }
            // declaration
            ved::Activity& a = addActivity("Fertilisation");
            // binding of the action related to the activity
            a.addOutputFunction(boost::bind(
                                    &Moderato::outFertilisation, this, _1, _2, _3));
            // connecting the rule to the activity
            decisionFertilisation.RdDDebut999(a);
        }
        //
        // planning the Irrigation activities
        //
        if(hasIrrigation) {
            try
            {
                decisionIrrigation.init(filepathIrrigation, time);

            } catch(exception& e) {
                throw vu::ModellingError(vle::fmt(_("[%1%] %2% while loading parameters \
from file %3% and section %4%")) % getModelName() % e.what() % filepathIrrigation);
            }
            // activity added only if needed
            if (decisionIrrigation.parametres.isRdDSemis) {
                {
                    ved::Activity& a = addActivity("IrrigationSemis_1");

                    a.addOutputFunction(boost::bind(
                                            &Moderato::outIrrigationSemis_1, this, _1, _2, _3));

                    decisionIrrigation.RdDSemisDebut_1_999(a);
                }
                {
                    ved::Activity& a = addActivity("IrrigationSemis");

                    a.addOutputFunction(boost::bind(
                                            &Moderato::outIrrigationSemis, this, _1, _2, _3));

                    decisionIrrigation.RdDSemisDebut_2_999(a);

                    a.addAcknowledgeFunction(
                        boost::bind(&Moderato::ackIrrigationSemis,this, _1, _2));
                }
                // activity added only if needed
                if (isRenseignee(decisionIrrigation.parametres.ArretSemis)) {
                    ved::Activity& a = addActivity("IrrigationSemisArret");

                    a.addOutputFunction(boost::bind(
                                            &Moderato::outIrrigationSemisArret, this, _1, _2, _3));

                    decisionIrrigation.RdDSemisArret999(a);
                }
            }

            {
                ved::Activity& a = addActivity("IrrigationDebut");

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationDebut, this, _1, _2, _3));

                decisionIrrigation.RdDDebut999(a);
            }

            {
                ved::Activity& a = addActivity("IrrigationRetour" +
                                              getSuffixName(mIrrigationRetourNumber));

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationRetour, this, _1, _2, _3));

                decisionIrrigation.RdDRetour999(a);

                a.addAcknowledgeFunction(
                    boost::bind(&Moderato::ackIrrigationRetour,this, _1, _2));
            }

            {
                ved::Activity& a = addActivity("StopIrrigationArret" +
                                              getSuffixName(mIrrigationRetourNumber));

                a.addOutputFunction(boost::bind(
                                        &Moderato::outStopIrrigationArret, this, _1, _2, _3));

                a.addAcknowledgeFunction(
                    boost::bind(&Moderato::ackStopIrrigationArret,this, _1, _2));
            }

            {
                ved::Activity& a = addActivity("IrrigationRetard" +
                                              getSuffixName(mIrrigationRetardNumber));

                decisionIrrigation.RdDRetardPluie999(a);

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationRetard, this, _1, _2, _3));
                a.addAcknowledgeFunction(
                    boost::bind(&Moderato::ackIrrigationRetard,this, _1, _2));
            }

            addStartToStartConstraint( "IrrigationRetour" +
                                        getSuffixName(mIrrigationRetourNumber),
                                        "StopIrrigationArret" +
                                        getSuffixName(mIrrigationRetourNumber),
                                        0.0, vd::Time::infinity);

            addStartToStartConstraint( "IrrigationRetour" +
                                       getSuffixName(mIrrigationRetourNumber),
                                       "IrrigationRetard" +
                                       getSuffixName(mIrrigationRetourNumber),
                                       0.0, vd::Time::infinity);

            addFinishToStartConstraint("Semis",
                                       "IrrigationDebut",
                                       0.0,
                                       vd::Time::infinity);

            // activity added only if needed
            if (decisionIrrigation.parametres.isRdDSemis) {

                addFinishToStartConstraint("Semis",
                                           "IrrigationSemis_1",
                                           0.0,
                                           vd::Time::infinity);

                addFinishToStartConstraint("IrrigationSemis_1",
                                           "IrrigationSemis",
                                           0.0,
                                           vd::Time::infinity);

                if (isRenseignee(decisionIrrigation.parametres.ArretSemis)) {
                    addStartToStartConstraint( "IrrigationSemis",
                                               "IrrigationSemisArret",
                                               0.0, vd::Time::infinity);
                }
            }

            addFinishToStartConstraint("IrrigationDebut",
                                       "IrrigationRetour" +
                                       getSuffixName(mIrrigationRetourNumber),
                                       0.0, vd::Time::infinity);

            {
                ved::Activity& a = addActivity("IrrigationArret" +
                                              getSuffixName(mIrrigationRetourNumber));

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationArret, this, _1, _2, _3));

                decisionIrrigation.RdDArret999(a);
            }

            {
                ved::Activity& a = addActivity("IrrigationArretArret" +
                                              getSuffixName(mIrrigationRetourNumber));;

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationArretArret, this, _1, _2, _3));

                decisionIrrigation.RdDArretIrrigArret999(a);
            }

            addStartToStartConstraint("IrrigationArret" +
                                      getSuffixName(mIrrigationRetourNumber),
                                      "IrrigationArretArret" +
                                      getSuffixName(mIrrigationRetourNumber),
                                      0.0, vd::Time::infinity);
        }

        if(hasRecolte) {
            addFinishToStartConstraint("Semis", "Recolte", 0.0, vd::Time::infinity);
        }

        return ved::Agent::init(time);
    }

    virtual ~Moderato() {}

    void outSemis(const std::string& name, const ved::Activity& activity,
                  vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {

            //std::cout << "[Agent Moderato]: outSemis" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent(name);
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value", new vv::Double(1.));
            out.addEvent(evt);

            out << (vee::Var("dateSemis") = currentTime());
        }

    }

    void outFertilisation(const std::string& name, const ved::Activity& activity,
                          vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {

            //std::cout << "[Agent Moderato]: outFertilisation" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent(name);
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value", new vv::Double(1.));
            out.addEvent(evt);
        }
    }

    void outRecolte(const std::string& name, const ved::Activity& activity,
                    vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {

            //std::cout << "[Agent Moderato]: outRecolte" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent(name);
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            out.addEvent(evt);

            out << (vee::Var("dateRecolte") = currentTime());

        }
    }

    void outIrrigationDebut(const std::string& name, const ved::Activity& activity,
                            vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            //std::cout << "[Agent Moderato]: outIrrigationDebut" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent(name);
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("QuantiteAppliquee", new vv::Double(clcDose(1)));
            out.addEvent(evt);
        }
    }

    void outIrrigationSemis(const std::string& name, const ved::Activity& activity,
                            vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            //std::cout << "[Agent Moderato]: outIrrigationSemis" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent(name);
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("QuantiteAppliquee", new vv::Double(clcDose(0)));
            out.addEvent(evt);
        }
    }

    void outIrrigationSemis_1(const std::string& name, const ved::Activity& activity,
                            vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            //std::cout << "[Agent Moderato]: outIrrigationSemis_1" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent(name);
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value", new vv::String("done"));
            out.addEvent(evt);

            out << (vee::Var("dateIrrigSemis") = currentTime());
        }
    }

    void outIrrigationSemisArret(const std::string& name,
                                 const ved::Activity& activity,
                                 vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {

            //std::cout << "[Agent Moderato]: outIrrigationSemisArret" << std::endl;

            vd::ExternalEvent* evt = new vd::ExternalEvent("IrrigationSemisArret");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            out.addEvent(evt);
        }
    }

    void outStopIrrigationArret(const std::string& name,
                                const ved::Activity& activity,
                                vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("StopIrrigationArret");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value", new vv::String("done"));
            out.addEvent(evt);
        }
    }

    void outIrrigationRetour(const std::string& name, const ved::Activity& activity,
                             vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("IrrigationRetour");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("QuantiteAppliquee", new vv::Double(clcDose(2)));
            out.addEvent(evt);

        }
    }

    void outIrrigationArret(const std::string& name, const ved::Activity& activity,
                            vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("IrrigationArret");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("QuantiteAppliquee", new vv::Double(clcDose(3)));
            out.addEvent(evt);
        }
    }

    void outIrrigationArretArret(const std::string& name,
                                 const ved::Activity& activity,
                                 vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("IrrigationArretArret");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            out.addEvent(evt);
        }
    }

    void outIrrigationRetard(const std::string& name, const ved::Activity& activity,
                             vd::ExternalEventList& out)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt1 = new vd::ExternalEvent("IrrigationRetard");
            evt1->putAttribute("name", new vv::String(name));
            evt1->putAttribute("activity", new vv::String(name));
            evt1->putAttribute("nbJoursRetard", new vv::Double(clcNbJoursRetard()));
            out.addEvent(evt1);

            out << (vee::Var("resetRetardPluie") = 0);
        }
    }

    double clcNbJoursRetard() const
    {
        return BdFaits.getnbjoursretard(decisionIrrigation.parametres.ValeursPluie[0],
                                        decisionIrrigation.parametres.ValeursPluie[1],
                                        decisionIrrigation.parametres.ValeursPluie[3]);
    }

    double clcDose(int numRegle) const
    {
	double valeur = 0;
	double Dose = 0;

        // numRegle reprend le type de règle:
        // 0=semis, 1=déclenchement, 2=retour, 3=arret
	if(numRegle == 0)
            valeur = decisionIrrigation.parametres.DoseSemis;
	else if(numRegle == 1)
            valeur = decisionIrrigation.parametres.DoseDebut;
	else if(numRegle == 2)
	{
            if(decisionIrrigation.parametres.qCondRetour == 1)
                valeur = decisionIrrigation.parametres.Tab1_7[0];
            else
            {
                if(BdFaits.STADE < FINCROISFEUIL_SORTIESOIES)
                    valeur = decisionIrrigation.parametres.Tab1_7[0];
                else if(BdFaits.STADE < AVORTGRAIN_DBTSEN)
                    valeur = decisionIrrigation.parametres.Tab1_7[1];
                else if(BdFaits.STADE < DBTSEN_PATEUXMOUX)
                    valeur = decisionIrrigation.parametres.Tab1_7[2];
                else
                    valeur = decisionIrrigation.parametres.Tab1_7[3];
            }
	}
	else if( numRegle == 3)
            valeur = decisionIrrigation.parametres.DoseArret;
	if(((numRegle == 0) && (decisionIrrigation.parametres.qDoseSemis == 1))
           ||((numRegle == 1) && (decisionIrrigation.parametres.qDoseDebut == 1 ))
           ||((numRegle == 2) && (decisionIrrigation.parametres.qDoseRetour == 1 ))
           ||((numRegle == 3) && (decisionIrrigation.parametres.qDoseArret == 1 ))) {

            Dose = valeur;
            return Dose;
	} else {
            int indicateur=0;
            switch(numRegle) {
            case 0:
                indicateur = decisionIrrigation.parametres.IndicateurDoseSemis;
                break;
            case 1:
                indicateur = decisionIrrigation.parametres.IndicateurDoseDebut;
                break;
            case 2:
                indicateur = decisionIrrigation.parametres.IndicateurDoseRetour;
                break;
            case 3:
                indicateur = decisionIrrigation.parametres.IndicateurDoseArret;
                break;
            }
            switch(indicateur) {
                //On veut remplir la réserve pour conserver un déficit donné
            case 1:
                Dose = (BdFaits.Frut > valeur) ? 0 :
                BdFaits.DeficitSaturationEau * (1 - (1. - valeur) / (1. - BdFaits.Frut));
                break;
            case 2:
                Dose = BdFaits.DeficitSaturationEau - valeur;
                if(Dose < 0)
                    Dose = 0;
                break;
            case 3:
                Dose = BdFaits.DeficitSaturationPmax - valeur;
                if( Dose < 0)
                    Dose = 0;
                break;
            }
	}
	return Dose;
    }

    virtual void ackIrrigationRetour(const std::string& /*activity*/,
                                     const ved::Activity& /*value*/)
    {
        removeActivity("IrrigationRetard" +
                       getSuffixName(mIrrigationRetardNumber));

        if (BdFaits.dateRecolte == vd::Time::infinity) {

            mIrrigationRetourNumber++;

            {
                ved::Activity& a = addActivity("IrrigationRetour" +
                                               getSuffixName(mIrrigationRetourNumber));
                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationRetour, this, _1, _2, _3));
                decisionIrrigation.RdDRetour999(a);

                a.addAcknowledgeFunction(
                    boost::bind(&Moderato::ackIrrigationRetour,this, _1, _2));
            }

            {
                ved::Activity& a = addActivity("StopIrrigationArret" +
                                               getSuffixName(mIrrigationRetourNumber));

                a.addOutputFunction(boost::bind(
                                        &Moderato::outStopIrrigationArret, this, _1, _2, _3));

                a.addAcknowledgeFunction(
                    boost::bind(&Moderato::ackStopIrrigationArret,this, _1, _2));
            }

            {
                ved::Activity& a = addActivity("IrrigationArret" +
                                               getSuffixName(mIrrigationRetourNumber));

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationArret, this, _1, _2, _3));

                decisionIrrigation.RdDArret999(a);
            }

            {
                ved::Activity& a = addActivity("IrrigationArretArret" +
                                               getSuffixName(mIrrigationRetourNumber));;

                a.addOutputFunction(boost::bind(
                                        &Moderato::outIrrigationArretArret, this, _1, _2, _3));

                decisionIrrigation.RdDArretIrrigArret999(a);
            }

            addStartToStartConstraint("IrrigationRetour" +
                                      getSuffixName(mIrrigationRetourNumber),
                                      "StopIrrigationArret" +
                                      getSuffixName(mIrrigationRetourNumber),
                                      0.0, vd::Time::infinity);

            addStartToStartConstraint("IrrigationArret" +
                                      getSuffixName(mIrrigationRetourNumber),
                                      "IrrigationArretArret" +
                                      getSuffixName(mIrrigationRetourNumber),
                                      0.0, vd::Time::infinity);
        }
    }

    virtual void ackIrrigationRetard(const std::string& /*activity*/,
                                     const ved::Activity& /*value*/)
    {
        mIrrigationRetardNumber++;
        {
            ved::Activity& a = addActivity("IrrigationRetard" +
                                          getSuffixName(mIrrigationRetardNumber));
            a.addOutputFunction(boost::bind(
                                    &Moderato::outIrrigationRetard, this, _1, _2, _3));
            decisionIrrigation.RdDRetardPluie999(a);

            a.addAcknowledgeFunction(
                boost::bind(&Moderato::ackIrrigationRetard,this, _1, _2));
        }
    }

    /**
     * @brief remove the IrrigationSemisArret activity if needed
     *
     * If the IrrigationSemisArret activity does not exist, this has no effect.
     * But if the IrrigationSemisArret exist, it is removed, and even
     * if not yet started.
     */
    virtual void ackIrrigationSemis(const std::string& /*activity*/,
                                    const ved::Activity& /*value*/)
    {
        //std::cout << "[Agent Moderato]: ackIrrigationSemis" <<std::endl;

        removeActivity("IrrigationSemisArret");
    }

    virtual void ackStopIrrigationArret(const std::string& /*activity*/,
                                        const ved::Activity& /*value*/)
    {
        removeActivity("IrrigationArret" +
                       getSuffixName(mIrrigationRetourNumber));

        removeActivity("IrrigationArretArret" +
                       getSuffixName(mIrrigationRetourNumber));
    }

    virtual void ackRecolte(const std::string& /*activity*/,
                            const ved::Activity& /*value*/)
    {
        // removeActivity("IrrigationRetour" +
        //               getSuffixName(mIrrigationRetourNumber));

        //removeActivity("IrrigationRetard" +
        //               getSuffixName(mIrrigationRetardNumber));

        removeActivity("IrrigationArret" +
                       getSuffixName(mIrrigationRetourNumber));

        removeActivity("IrrigationArretArret" +
                       getSuffixName(mIrrigationRetourNumber));
    }

    void removeActivity(const std::string& name)
    {
        //std::cout << "[Agent Moderato]: removeActivity" <<std::endl;

        if (activities().exist(name)) {
            const ved::Activity& a = activities().get(name)->second;
            if (!a.isInDoneState()) {
                //std::cout << "[Agent Moderato]: removeActivity,setActivityFailed : " << name <<std::endl;
                setActivityFailed(name, currentTime());
            }
        }
    }

    std::string getSuffixName(int n) const
    {
    	std::stringstream ret;
    	ret << "_";
    	ret << boost::format("%1$02d") % n;
    	return ret.str();
    }

private:

    string filepathSemis;
    string filepathFertilisation;
    string filepathIrrigation;
    string filepathRecolte;

    bool hasSemis;
    bool hasFertilisation;
    bool hasIrrigation;
    bool hasRecolte;

    Faits BdFaits;

    DecisionSemis decisionSemis;
    DecisionFertilisation decisionFertilisation;
    DecisionIrrigation decisionIrrigation;
    DecisionRecolte decisionRecolte;

    int mIrrigationRetourNumber;
    int mIrrigationRetardNumber;

    vd::Time mTime;

};

} // namespace Moderato

DECLARE_DYNAMICS(Moderato::Moderato)

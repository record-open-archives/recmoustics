/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "ParametresContexte.hpp"

using namespace std;

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace po = boost::program_options;

namespace Moderato {

    typedef vector< string > split_vector_type;

    void ParametresContexte::init(const string paramfile, const vd::Time& time)
    {

        ifstream config(paramfile.c_str());

        if (not config) {
            throw vu::ModellingError(
                vle::fmt(_("ParametresContexte - cannot"	\
                           " open file : %1%"))
                % paramfile);
        }

        po::options_description desc("Options");
        desc.add_options()
            ("Indicateur.Position", po::value<int>(), "")
            ("Contexte.JoursSans", po::value<string>(), "")
            ("Contexte.TauxMax", po::value<double>(), "")
            ("Contexte.DebitMax", po::value<double>(), "")
            ("Contexte.DoseMini", po::value<double>(), "")
            ("Contexte.DoseMaxi", po::value<double>(), "")
            ("Contexte.VolumeLimite", po::value<double>(), "");

        for (int i = 1; i < NB_CONTRAINTES - 1 ; i++) {
            string tmpParam0 = "Contexte.DebitTauxDeA"+ lexical_cast<string>(i);
            string tmpParam1 = "Contexte.JoursSans"+ lexical_cast<string>(i);

            desc.add_options()
                (tmpParam0.c_str(), po::value<string>(), "")
                (tmpParam1.c_str(), po::value<string>(), "");
                }

        po::variables_map vm;
        po::store(po::parse_config_file(config, desc, true), vm);
        po::notify(vm);

        string section = "Indicateur.";

        Position = vm[section + "Position"].as<int>();

        section = "Contexte.";

        split_vector_type SplitVec;

        TContraintes uneContrainte;

        string joursSans = vm[section + "JoursSans"].as<string>();

        uneContrainte.Numero = 0;
        uneContrainte.Taux = vm[section + "TauxMax"].as<double>();
        uneContrainte.Debit = vm[section + "DebitMax"].as<double>();
        uneContrainte.DateDebut = vd::Time::negativeInfinity;
        uneContrainte.DateFin = vd::Time::infinity;

        split( SplitVec, joursSans, is_any_of(",") );
        for (int i = 0; i < NB_JOURS_SEMAINE; i++) {
            if (SplitVec[i] == "0"){
                uneContrainte.JoursSansIrrigation[i] = false;
            } else {
                uneContrainte.JoursSansIrrigation[i] = true;
            }
        }

        mesContraintes.push_back(uneContrainte);

        DoseMinimale = vm[section + "DoseMini"].as<double>();
        DoseMaximale = vm[section + "DoseMaxi"].as<double>();
        VolumeTotal = vm[section + "VolumeLimite"].as<double>();

        for (int i = 1; i < NB_CONTRAINTES - 1 ; i++) {
            string debiTauxDeA = vm[section +
                                    "DebitTauxDeA" +
                                    lexical_cast<string>(i)].as<string>();

            joursSans = vm[section +
                           "JoursSans" +
                           lexical_cast<string>(i)].as<string>();

            split( SplitVec, debiTauxDeA, is_any_of(","));
            // si l'interval est défini
            if  (isRenseignee(SplitVec[2]) && isRenseignee(SplitVec[3])) {

                uneContrainte.Numero = i;

                uneContrainte.Debit = lexical_cast<double>(SplitVec[0]);
                uneContrainte.Taux = lexical_cast<double>(SplitVec[1]);
                uneContrainte.DateDebut = toTime(SplitVec[2],time);
                uneContrainte.DateFin = toTime(SplitVec[3],time);

                split( SplitVec, joursSans, is_any_of(",") );

                for (int j = 0; j < NB_JOURS_SEMAINE; j++) {
                    if (SplitVec[j] == "0"){
                        uneContrainte.JoursSansIrrigation[j] = false;
                    } else {
                        uneContrainte.JoursSansIrrigation[j] = true;
                    }
                }
                mesContraintes.push_back(uneContrainte);
            }
        }
    };

    int ParametresContexte::getContext(const vle::devs::Time& time) const
    {
	vector< TContraintes >::const_iterator itc = mesContraintes.begin();
	vector< TContraintes >::const_iterator itcres;

	// to start on the first additionnal context.
	itc++;

	itcres = find_if(itc, mesContraintes.end(), in_context(time));

	if (itcres == mesContraintes.end())
	    return 0;
	else
	    return itcres->Numero;
    }

} // namespace Moderato


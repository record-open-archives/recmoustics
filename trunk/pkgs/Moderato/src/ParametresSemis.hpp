/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_PARAMETRESSEMIS_HPP
#define MODERATO_PARAMETRESSEMIS_HPP

#include <string>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include <boost/program_options.hpp>

#include "Utils.hpp"

namespace vu = vle::utils;
namespace vd = vle::devs;

namespace Moderato {
/**
 * @brief Parameters and values of the sowing
 *
 * This class is a simple structure containing all the parameters,
 * and their values used by the sowing rule,
 * and the sowing operation.
 */
class ParametresSemis
{
public:

    ParametresSemis() :
        DateMin(0.), DateMax(0.), Densite(0.), Variete(0),
        SommePluie(0.), nbJours(0), Frut(0.), IndicateurSol(0)
    { };

    virtual ~ParametresSemis() { };

    /**
     * @brief Setting of the parameters
     *
     * @param paramfile is the complete path to the config file
     * @param time is an access to the year of the simulation
     */
    void init(const std::string paramfile, const vd::Time& time);

    vd::Time DateMin;
    vd::Time DateMax;
    double Densite;
    int Variete;
    double SommePluie;
    int nbJours;
    double Frut;
    int IndicateurSol;
};

} // namespace Moderato

#endif


/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "ParametresIrrigation.hpp"

using namespace std;

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace po = boost::program_options;

namespace Moderato {

    typedef vector< string > split_vector_type;

    void ParametresIrrigation::init(const string paramfile, const vd::Time& time)
    {

        ifstream config(paramfile.c_str());

        if (not config) {
            throw vu::ModellingError(
                vle::fmt(_("ParametresIrrigation - cannot"	\
                           " open file : %1%"))
                % paramfile);
        }

        po::options_description desc("Options");
        desc.add_options()
            ("RdDSemis.isRdDSemis", po::value<bool>(), "")
            ("RdDSemis.nbJours", po::value<int>(), "")
            ("RdDSemis.SommeTemp", po::value<double>(), "")
            ("RdDSemis.isQCond", po::value<int>(), "")
            ("RdDSemis.IndicateurSol", po::value<int>(), "")
            ("RdDSemis.SommePluie", po::value<double>(), "")
            ("RdDSemis.Frut", po::value<double>(), "")
            ("RdDSemis.isQDose", po::value<int>(), "")
            ("RdDSemis.IndicateurDose", po::value<int>(), "")
            ("RdDSemis.Dose", po::value<double>(), "")
            ("RdDSemis.ArrêtTourEau", po::value<double>(), "")
            ("RdDSemis.isARetirer", po::value<bool>(), "")
            ("RdDDebut.Date", po::value<string>(), "")
            ("RdDDebut.SommeTemp", po::value<double>(), "")
            ("RdDDebut.Operateur", po::value<bool>(), "")
            ("RdDDebut.SommeTempMax", po::value<double>(), "")
            ("RdDDebut.isQCond", po::value<int>(), "")
            ("RdDDebut.IndicateurSol", po::value<int>(), "")
            ("RdDDebut.ValeursPluie", po::value<string>(), "")
            ("RdDDebut.ValeursFrut", po::value<string>(), "")
            ("RdDDebut.isQDose", po::value<int>(), "")
            ("RdDDebut.IndicateurDose", po::value<int>(), "")
            ("RdDDebut.Dose", po::value<double>(), "")
            ("RdDRetour.isQCond", po::value<int>(), "")
            ("RdDRetour.isQCond2", po::value<int>(), "")
            ("RdDRetour.IndicateurSol", po::value<int>(), "")
            ("RdDRetour.isQDose", po::value<int>(), "")
            ("RdDRetour.IndicateurDose", po::value<int>(), "")
            ("RdDRetour.isModulation", po::value<bool>(), "")
            ("RdDRetour.isF1egF21", po::value<bool>(), "")
            ("RdDRetour.isF24egF3", po::value<bool>(), "")
            ("RdDRetour.Tab1_1", po::value<string>(), "")
            ("RdDRetour.Tab1_2", po::value<string>(), "")
            ("RdDRetour.Tab1_3", po::value<string>(), "")
            ("RdDRetour.Tab1_4", po::value<string>(), "")
            ("RdDRetour.Tab1_5", po::value<string>(), "")
            ("RdDRetour.Tab1_6", po::value<string>(), "")
            ("RdDRetour.Tab1_7", po::value<string>(), "")
            ("RdDRetour.Tab2_1", po::value<string>(), "")
            ("RdDRetour.Tab2_2", po::value<string>(), "")
            ("RdDRetour.Tab2_3", po::value<string>(), "")
            ("RdDRetour.Tab2_4", po::value<string>(), "")
            ("RdDRetour.Tab2_5", po::value<string>(), "")
            ("RdDRetard.isRdDRetard", po::value<bool>(), "")
            ("RdDRetard.isPluie", po::value<bool>(), "")
            ("RdDRetard.valeursPluie", po::value<string>(), "")
            ("RdDRetard.isDecouple", po::value<bool>(), "")
            ("RdDRetard.isRetour1", po::value<bool>(), "")
            ("RdDRetard.valeurRetour1", po::value<double>(), "")
            ("RdDRetard.isVent", po::value<bool>(), "")
            ("RdDRetard.valeursVent", po::value<string>(), "")
            ("RdDArret.Date", po::value<string>(), "")
            ("RdDArret.SommeTemp", po::value<double>(), "")
            ("RdDArret.isQCond", po::value<int>(), "")
            ("RdDArret.IndicateurSol", po::value<int>(), "")
            ("RdDArret.valeursClimat", po::value<string>(), "")
            ("RdDArret.Frut", po::value<double>(), "")
            ("RdDArret.isQDose", po::value<int>(), "")
            ("RdDArret.IndicateurDose", po::value<int>(), "")
            ("RdDArret.Dose", po::value<double>(), "")
            ("RdDArret.ArrêtTourEau", po::value<double>(), "")
            ("RdDAnticipation.isRdDAnticipation", po::value<bool>(), "")
            ("RdDAnticipation.SommePluie", po::value<double>(), "")
            ("RdDAnticipation.nbJours", po::value<int>(), "")
            ("RdDAnticipation.nbJoursReport", po::value<int>(), "");

        po::variables_map vm;
        po::store(po::parse_config_file(config, desc, true), vm);
        po::notify(vm);

        string section = "RdDSemis.";

        isRdDSemis = vm[section + "isRdDSemis"].as<bool>();
        nbJoursSemis = vm[section + "nbJours"].as<int>();
        SommeTempSemis = vm[section + "SommeTemp"].as<double>();
        qCondSemis = vm[section + "isQCond"].as<int>();
        IndicateurSolSemis = vm[section + "IndicateurSol"].as<int>();
        SommePluieSemis = vm[section + "SommePluie"].as<double>();
        FrutSemis = vm[section + "Frut"].as<double>();
        qDoseSemis  = vm[section + "isQDose"].as<int>();
        IndicateurDoseSemis  = vm[section + "IndicateurDose"].as<int>();
        DoseSemis = vm[section + "Dose"].as<double>();
        ArretSemis = vm[section + "ArrêtTourEau"].as<double>();
        isARetirerSemis = vm[section + "isARetirer"].as<bool>();

        section = "RdDDebut.";

        DateDebut = toTime(vm[section + "Date"].as<string>(),
                           time);
        SommeTempDebut = vm[section + "SommeTemp"].as<double>();
        isOU_ET = vm[section + "Operateur"].as<bool>();
        SommeTempDebutMax = vm[section + "SommeTempMax"].as<double>();
        qCondDebut = vm[section + "isQCond"].as<int>();
        IndicateurSolDebut = vm[section + "IndicateurSol"].as<int>();
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "ValeursPluie"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 3; i++) {
                ValeursPluieDebut[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "ValeursFrut"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 7; i++) {
                ValeursFrutDebut[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        qDoseDebut  = vm[section + "isQDose"].as<int>();
        IndicateurDoseDebut = vm[section + "IndicateurDose"].as<int>();
        DoseDebut = vm[section + "Dose"].as<double>();

        section = "RdDRetour.";

        qCondRetour = vm[section + "isQCond"].as<int>();
        qCond2Retour = vm[section + "isQCond2"].as<int>();
        IndicateurSolRetour = vm[section + "IndicateurSol"].as<int>();
        qDoseRetour = vm[section + "isQDose"].as<int>();
        IndicateurDoseRetour = vm[section + "IndicateurDose"].as<int>();
        isModulation = vm[section + "isModulation"].as<bool>();
        isF1egF21 = vm[section + "isF1egF21"].as<bool>();
        isF24egF3 = vm[section + "isF24egF3"].as<bool>();
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_1"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_1[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_2"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_2[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_3"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_3[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_4"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_4[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_5"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_5[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_6"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_6[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab1_7"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab1_7[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab2_1"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab2_1[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab2_2"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab2_2[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab2_3"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab2_3[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab2_4"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab2_4[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "Tab2_5"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 4; i++) {
                Tab2_5[i] = lexical_cast<double>(SplitVec[i]);
            }
        }

        section = "RdDRetard.";

        isRdDRetard = vm[section + "isRdDRetard"].as<bool>();
        isCondPluieRetard = vm[section + "isPluie"].as<bool>();
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "valeursPluie"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 5; i++) {
                ValeursPluie[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        isDecouple = vm[section + "isDecouple"].as<bool>();
        isRetour1Retard = vm[section + "isRetour1"].as<bool>();
        Retour1Retard = vm[section + "valeurRetour1"].as<double>();
        isCondVentRetard = vm[section + "isVent"].as<bool>();
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "valeursVent"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 2; i++) {
                ValeursVent[i] = lexical_cast<double>(SplitVec[i]);
            }
        }

        section = "RdDArret.";

        DateArret = toTime(vm[section + "Date"].as<string>(),
                           time);
        SommeTempArret = vm[section + "SommeTemp"].as<double>();
        qCondArret = vm[section + "isQCond"].as<int>();
        IndicateurSolArret = vm[section + "IndicateurSol"].as<int>();
        {
            split_vector_type SplitVec;
            string tmp = vm[section + "valeursClimat"].as<string>();
            split( SplitVec, tmp, is_any_of(",") );
            for (int i = 0; i < 7; i++) {
                ValeursClimatArret[i] = lexical_cast<double>(SplitVec[i]);
            }
        }
        FrutArret = vm[section + "Frut"].as<double>();
        qDoseArret = vm[section + "isQDose"].as<int>();
        IndicateurDoseArret = vm[section + "IndicateurDose"].as<int>();
        DoseArret  = vm[section + "Dose"].as<double>();
        ArretArret  = vm[section + "ArrêtTourEau"].as<double>();

        section = "RdDAnticipation.";

        isRdDAnticipation  = vm[section + "isRdDAnticipation"].as<bool>();
        ValeurPluieAnticipation = vm[section + "SommePluie"].as<double>();
        nbJoursPluieAnticipation = vm[section + "nbJours"].as<int>();
        nbJoursReportAnticipation = vm[section + "nbJoursReport"].as<int>();
    };

} // namespace Moderato


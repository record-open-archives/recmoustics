/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MODERATO_UTILS_HPP
#define MODERATO_UTILS_HPP

#include <string>
#include <boost/algorithm/string.hpp>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace vu = vle::utils;
namespace vd = vle::devs;

namespace Moderato {

    enum STADEPHYSIO {AUCUN, SEMIS_LEVEE, LEVEE_FINCROISFEUIL,
                      FINCROISFEUIL_SORTIESOIES,
                      SORTIESOIES_AVORTGRAIN, AVORTGRAIN_DBTSEN,
                      DBTSEN_PATEUXMOUX, PATEUXMOUX_MATPHYSIO,
                      MATURE};

    vd::Time toTime(const std::string paramTime,
                    const vd::Time& time);

    bool isRenseignee(const double val);

    bool isRenseignee(const int val);

    bool isRenseignee(const std::string val);

} // namespace Moderato

#endif


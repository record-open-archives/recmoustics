/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/devs/Dynamics.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>
#include <vle/vpz.hpp>
#include <boost/numeric/conversion/cast.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;

namespace Moderato {
/**
 * @brief a activities vle::devs::Dynamics model
 *
 * A very simple model to observe all activities started one day.
 * The state of the model is a simple string. In this string all the
 * events(activities) launched the day are concatenated. This model is util,
 * and convenient to observe this aspect of the system.
 *
 * the list of ports :
 * - Experimental condition :
 * - Input :
 *  - "no specific name required" : but only Difference Equation
 * values are expected
 * - Output :
 * - Observation :
 *  - Activities
 */
class ObservatorActivity: public vd::Dynamics
{

public:
    ObservatorActivity(const vd::DynamicsInit& mdl,
                       const vd::InitEventList& evts)
        : vd::Dynamics(mdl, evts), mTime(vd::Time::infinity)
    {};

    virtual ~ObservatorActivity() { }

    virtual vd::Time init(const vd::Time& /*time*/)
    {
        return vd::Time::infinity;
    };

    vd::Time timeAdvance() const
    {
        return vd::Time::infinity;
    };

    void externalTransition(
        const vd::ExternalEventList& events,
        const vd::Time& time)
    {
        // to empty the string gathering operations from the previous day
        if (time != mTime) {
            mActivities.clear();
            mTime = time;
        }

        // to concatenate the activities
        for (vd::ExternalEventList::const_iterator it = events.begin();
             it != events.end(); ++it) {

            mActivities += str(boost::format(" [Activité %s]")
                               % (*it)->getStringAttributeValue("activity"));
        }
    };

    vv::Value* observation(const vd::ObservationEvent& event) const
    {
	if (event.onPort("Activities")) return buildString(mActivities);

        return 0;
    }

protected:

    vd::Time mTime;

    std::string mActivities;
};

} // namespace Moderato

DECLARE_DYNAMICS(Moderato::ObservatorActivity)
